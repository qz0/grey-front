import { Injectable } from '@angular/core';
import {DataService} from '../_services/data.service';
import {Cart, CartItem} from '../_models/cart.types';
import {Product} from '../_models/product.types';
import {OptionValue} from '../_models/option.types';
import {Delivery, Zone, ZoneCost} from '../_models/delivery.types';
import {Address, Country} from '../_models/address.types';
import {Order} from '../_models/order.types';
import {GenerateService} from '../_services/generate.service';
import {catchError, map, tap} from 'rxjs/operators';
import {Observable, of} from 'rxjs';
import {Currency} from '../_models/currency.types';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  //  public
  public cart: Cart = null;   //  текущая корзина
  public delivery: Delivery = null;   //  текущая доставка

  constructor(private dataService: DataService) {
    //  подписываемся на корзину
    this.dataService.castCart.subscribe((cart: Cart) => this.cart = cart);
    //  подписываемся на доставки
    this.dataService.castDelivery.subscribe((delivery: Delivery) => this.delivery = delivery);
  }

  //  добавить товар в корзину
  //  product - товар
  //  optionValues - опции
  //  amount - количество
  public addProductToCart(product: Product, optionValues: OptionValue[], amount: number = 1) {
    this.dataService.addProductToCart(product, optionValues, amount);
  }

  //  dec cartItem
  public decCartItem(cartItemIndex: number) {
    //  если индекс внутри масива
    if (this.cart.cartItems && cartItemIndex < this.cart.cartItems.length) {
      //  уменьшаем количество
      if (this.cart.cartItems[cartItemIndex].orderCount > 0) {
        this.cart.cartItems[cartItemIndex].orderCount--;
      }
      //  меняем цену доставки
      this.getDeliveryCost(this.cart);
      //  публикуем корзину
      this.dataService.publishCart(this.cart);
    }
  }

  //  создание нового ордера
  public createOrder(deliveryID?: number, paymentID?: number, address?: Address, currencyID?: number, cartItems?: CartItem[]):
    Observable<Order> {
    //  создаем пустой заказ
    const newOrder: Order = GenerateService.createOrder();
    //  передаем значения
    if (deliveryID) {
      newOrder.deliveryID = deliveryID;
    }
    if (paymentID) {
      newOrder.paymentID = paymentID;
    }
    if (address) {
      newOrder.createdShippingAddress = address;
    }
    if (currencyID) {
      newOrder.currencyID = currencyID;
    }
    if (cartItems && cartItems.length) {
      newOrder.cartItems = cartItems;
    }
    //  дергаем сервис
    return this.dataService.createOrder(newOrder)
      .pipe(
        // map(orderServer => this.convertService.orderServerToClient(orderServer)),
        tap((order: Order) => order),
        catchError(err => {
          console.log(err, 'ERROR in createOrder service');
          return of(null);
        })
      );
  }

  //  получаем страну по ид
  //  countryID - id страны
  public getCountryByID(countryID: number) {
    //  дергаем сервис
    this.dataService.getCountryByID(String(countryID));

  }

  //  получаем доставку по ид
  //  deliveryID - id доставки
  public getDeliveryByID(deliveryID: number) {
    //  дергаем сервис
    this.dataService.getDeliveryByID(String(deliveryID));

  }

  //  получаем цену доставки
  public getDeliveryCost(cart: Cart) {
    console.log('getDeliveryCost', cart);
    //  если не пустой
    if (cart && cart.deliveryID && cart.cartItems && cart.cartItems.length) {
      //  дергаем сервис
      this.dataService.getDeliveryCost(cart);
    }

  }

  //  получаем регионы по стране
  //  countryID - id страны
  public getRegionsByCountryID(countryID: number) {
    //  дергаем сервис
    this.dataService.getRegionsByCountryID(String(countryID));
  }


  //  получаем оплату по ид
  //  paymentID - id оплаты
  public getPaymentByID(paymentID: number) {
    //  дергаем сервис
    this.dataService.getPaymentByID(String(paymentID));
  }


  //  получаем регион по ид
  //  regionID - id региона
  public getRegionByID(regionID: number) {
    //  дергаем сервис
    this.dataService.getRegionsByCountryID(String(regionID));
  }

  //  по стране и общему весу
  // public getDeliveryCosts(countryID: number, productsWeight: number): number {
  //   //  создаем переменные
  //   const zoneCosts: ZoneCost[] = this.getZoneCostsByCountryID(countryID);
  //   let zoneCost: ZoneCost = null;
  //   if (zoneCosts !== null) {
  //     zoneCost = zoneCosts[0];
  //   }
  //   //  начинаем искать наименьший подходящий вес
  //   zoneCosts.forEach((curZoneCost: ZoneCost) => {
  //     if (productsWeight <= curZoneCost.weight && curZoneCost.weight < zoneCost.weight) {
  //       zoneCost = curZoneCost;
  //     }
  //   });
  //   //  если удалось что-то присвоить - возвращаем вес, если нет - нал
  //   if (zoneCost) {
  //     return zoneCost.cost;
  //   } else {
  //     return null;
  //   }
  // }

  //  inc cartItem
  public incCartItem(cartItemIndex: number) {
    //  если индекс внутри масива
    if (this.cart.cartItems && cartItemIndex < this.cart.cartItems.length) {
      //  увеличиваем количество
      if ( this.cart.cartItems[cartItemIndex].orderCount < 999) {
        this.cart.cartItems[cartItemIndex].orderCount++;
      }
      //  меняем цену доставки
      this.getDeliveryCost(this.cart);
      //  публикуем корзину
      this.dataService.publishCart(this.cart);
    }
  }

  //  публикуем измененную корзину
  //  cart - новая корзина
  public publishCart(cart: Cart) {
    //  дергаем сервис
    this.dataService.publishCart(cart);
  }

  public removeProductFromCart(cartItem?: CartItem) {
    //  если передали айтем - удаляем конкретный продукт айтем, если нет - все
    if (cartItem && cartItem.id) {

    } else {
      //  обнуляем содержимое корзины
      this.cart.cartItems = [];
      //  публикуем корзину
      this.dataService.publishCart(this.cart);
    }
  }

  //  получение цен на веса зоны по ID страны
  private getZoneCostsByCountryID(countryID: number): ZoneCost[] {
    //  если есть доставка пробуем получить зону
    if (this.delivery && this.delivery.zones && this.delivery.zones.length) {
      //  перебираем
      this.delivery.zones.forEach((zone: Zone) => {
        //  пробуем найти индекс страны
        if (zone.countries.findIndex((country: Country) => country.id === countryID) !== -1) {
          return [...zone.zoneCosts];
        }
      });
      return null;
    }
  }
}
