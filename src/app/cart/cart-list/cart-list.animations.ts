// animations.ts
import { trigger, state, style, transition, animate } from '@angular/animations';

export const CartListAnimations = {
  // показать/скрыть блок корзины
  showHideCart: trigger('showHideCart', [
    transition(':enter', [
      style({ opacity: 0, transform: 'translateX(39rem)' }),
      animate('.3s', style({ opacity: 1, transform: 'translateX(0)' })),
    ]),
    transition(':leave', [
      style({ opacity: 1, transform: 'translateX(0)' }),
      animate('.3s', style({ opacity: 0, transform: 'translateX(39rem)' }))
    ])
  ])
};
