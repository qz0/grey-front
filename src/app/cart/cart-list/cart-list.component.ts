import {Component, OnDestroy, OnInit} from '@angular/core';
import {CartListAnimations} from './cart-list.animations';
import {CartService} from '../cart.service';
import {DataService} from '../../_services/data.service';
import {Cart, CartItem} from '../../_models/cart.types';
import {Subscription} from 'rxjs';
import {CurrencyService} from '../../_services/currency.service';
import {Currency} from '../../_models/currency.types';
import {Language, Localization} from '../../_models/localization.types';


@Component({
  selector: 'app-cart-list',
  animations: [CartListAnimations.showHideCart],
  templateUrl: './cart-list.component.html',
  styleUrls: ['./cart-list.component.scss']
})
export class CartListComponent implements OnInit, OnDestroy {
  // public
  public cart: Cart = null; // cart
  public currency: Currency = null; // cart
  public subTotalPrice: number = 0; //  предварительная цена всех товаров без доставки
  public localizations: Localization[] = null; // список локализаций
  public cartItemsAmount: number = null;

  // private
  private cartSub: Subscription = null;
  private currencySub: Subscription = null;
  private localizationsSub: Subscription; //  подписка на список локализаций

  constructor(private cartService: CartService, private dataService: DataService, private currencyService: CurrencyService) {
    // subscription
    //  подписка cart
    this.cartSub = this.dataService.castCart.subscribe((cart: Cart) => {
      if (cart) {
        this.cart = cart;
        console.log(this.cart)
        //  обновляем цену товара
        this.renewSubtotalPrice();

        //  если что-то есть - возвращаем число
        this.cartItemsAmount = 0;
        if (cart.cartItems && cart.cartItems.length) {
          //  пробегаемся по всем айтемам чтобы суммировать количество
          cart.cartItems.forEach((cartItem: CartItem) => {
            this.cartItemsAmount += cartItem.orderCount;
          });
        }
      }
    });
    //  подписка cart
    this.currencySub = this.dataService.castCurrency.subscribe(currency => this.renewSubtotalPrice()
    );

    //  подписка localizations
    this.localizationsSub = this.dataService.castLocalizations.subscribe((localizations: Localization[]) => {
      this.localizations = localizations;
    });

  }

  // destroy
  ngOnDestroy() {
    this.cartSub.unsubscribe();
    this.currencySub.unsubscribe();
    this.localizationsSub.unsubscribe();
  }

  // init
  ngOnInit() {
    //  пересчет цены
    this.renewSubtotalPrice();
  }

  //  удалить все cartItems из carts
  public removeAll() {
    //  дергаем сервис удаления без параметров
    this.cartService.removeProductFromCart();
    // localStorage.removeItem('cart');
  }

  //  сумма всех cartItems
  public renewSubtotalPrice() {
    let total: number = 0;
    if (this.cart.cartItems != null) {
      for (const cartItem of this.cart.cartItems) {
        total += this.currencyService.convertToCurrency(cartItem.product.price.sell) * cartItem.orderCount;
      }
    }
    this.subTotalPrice = total;

  }

  // закрывает корзину
  public hideCheckout() {
    this.dataService.changeCartVisibility(false);
  }
}
