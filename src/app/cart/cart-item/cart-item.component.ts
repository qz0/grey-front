import {Component, Input, OnDestroy, OnInit, ViewChild, ElementRef} from '@angular/core';
import {baseProto, imagesURL} from '../../_models/urls';
import {Cart, CartItem} from '../../_models/cart.types';
import {Subscription} from 'rxjs';
import {CartService} from '../cart.service';
import {DataService} from '../../_services/data.service';
import { Router } from '@angular/router';
import {Language} from '../../_models/localization.types';

@Component({
  selector: 'app-cart-item',
  templateUrl: './cart-item.component.html',
  styleUrls: ['./cart-item.component.scss']
})
export class CartItemComponent implements OnInit, OnDestroy {
  // public
  @Input() cartItem: CartItem = null; //  текущий товар
  @Input() cartItemIndex: number = null;  //  индекс текущего товара
  // поле для ввода количества товара
  @ViewChild('productCountInput', {static: false}) productCountInput: ElementRef;
  public cart: Cart = null; // cart
  public imagePath: string = baseProto + imagesURL + '/products/icon/';
  public optionsImagePath: string = baseProto + imagesURL;
  public languageString: string = 'EN';

  // private
  private cartSub: Subscription = null;
  private languageSub: Subscription;

  constructor(private cartService: CartService, private dataService: DataService,  private router: Router) {
    // subscription
    //  подписка cart
    this.cartSub = this.dataService.castCart.subscribe((cart: Cart) => {
      if (cart) {
        this.cart = cart;
      }
    });

    //  подписываемся на язык
    this.languageSub = this.dataService.castLanguage.subscribe((language: Language) => {
      if (language) {
        // console.log('Have changes :', language);
        this.languageString = language.language;
        console.log(this.languageString);
      }
    });
  }

  ngOnDestroy() {
    this.cartSub.unsubscribe();
  }

  ngOnInit() {
  }

  // add cartItem to cart
  public incCartItem() {
    //  дергаем сервис
    this.cartService.incCartItem(this.cartItemIndex);
  }

  // remove whole cart item
  public decCartItem() {
    //  дергаем сервис
    this.cartService.decCartItem(this.cartItemIndex);
  }

  // remove one cartItem from cart
  public removeCartItem() {
    //  если такой индекс есть
    if (this.cart.cartItems && this.cartItemIndex < this.cart.cartItems.length) {
      // delete whole cartItem from cart
      this.cart.cartItems.splice(this.cartItemIndex, 1);
      //  публикуем изменения
      this.dataService.publishCart(this.cart);
    }
  }

  // перейти к продукту
  public openProduct() {
    this.router.navigate(['/products/product/' + String(this.cartItem.product.id)]);
    this.dataService.changeCartVisibility(false);
  }

  // отсекает ввод букв в поле
  public checkOnSymbols(event) {
    const key = Number(event.key);
    // прерывать если введено не число кроме Backspace
    if ((isNaN(key) || event.key === null) && event.key !== 'Backspace') {
      event.preventDefault();
      return;
    }

  // если есть четыре символа прерывать ввод новых кроме Backspace
    if ( this.cartItem.orderCount.toString().length === 4 && event.key !== 'Backspace') {
      event.preventDefault();
    }
  }

  // изменение количества товра вручную
  public changeAmountOfProduct() {

    // удалены все символы - подставлять ноль
    if (this.productCountInput.nativeElement.value.length === 0) {
      this.cartItem.orderCount = 0;
      this.dataService.publishCart(this.cart);
      return;
    }

    // если введенная величина больше трех симоволов - сбрасывать до 999
    if (this.productCountInput.nativeElement.value.length > 3) {
      this.cartItem.orderCount = 999;
      this.dataService.publishCart(this.cart);
      return;
    }

    // присвоить введенное значение
    this.cartItem.orderCount = parseInt(this.productCountInput.nativeElement.value, 10);
    this.dataService.publishCart(this.cart);
  }


}
