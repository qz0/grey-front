import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CheckOutComponent } from './check-out/check-out.component';
import { CartListComponent } from './cart-list/cart-list.component';
import { CartItemComponent } from './cart-item/cart-item.component';
// import {CardComponent} from './check-out/card/card.component';
import {RouterModule} from '@angular/router';
import {PipesModule} from '../_pipes/pipes.module';
import {FormsModule} from '@angular/forms';
import {NgxPayPalModule} from 'ngx-paypal';
import {DirectivesModule} from '../__directives/directives.module';
import {NgSelectModule} from '@ng-select/ng-select';
import {PaymentModule} from '../payment/payment.module';




@NgModule({
  declarations: [CheckOutComponent, CartListComponent, CartItemComponent],
  exports: [
    CartListComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    PipesModule,
    FormsModule,
    NgxPayPalModule,
    NgSelectModule,
    DirectivesModule,
    PaymentModule
  ]
})
export class CartModule { }
