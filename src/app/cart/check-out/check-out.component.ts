import {Component, OnDestroy, OnInit, ViewChild, ViewChildren, ElementRef, ChangeDetectorRef, QueryList} from '@angular/core';
import { Animations } from '../../animations';
import {Cart, CartItem} from '../../_models/cart.types';
import {Subscription} from 'rxjs';
import {CartService} from '../cart.service';
import {DataService} from '../../_services/data.service';
import {Delivery} from '../../_models/delivery.types';
import {Payment} from '../../_models/payment.types';
import {Image} from '../../_models/image.types';
import {baseProto, imagesURL} from '../../_models/urls';
import {Address, Country, CountryShort, Region} from '../../_models/address.types';
import {IClientAuthorizeCallbackData, ICreateOrderRequest, IPayPalConfig, IPurchaseUnit, ITransactionItem, IUnitAmount} from 'ngx-paypal';
import {PaymentService} from '../../payment/payment.service';
import {Order} from '../../_models/order.types';
import {Currency} from '../../_models/currency.types';
import {GenerateService} from '../../_services/generate.service';
import {CurrencyService} from '../../_services/currency.service';
import {Language, Localization} from '../../_models/localization.types';

@Component({
  selector: 'app-check-out',
  animations: [Animations.fadeInOut],
  templateUrl: './check-out.component.html',
  styleUrls: ['./check-out.component.scss']
})
export class CheckOutComponent implements OnInit, OnDestroy {
  // public
  public showComment: boolean = false; // show/hide textarea for comment
  public showPaymentBlock: boolean = false; // show/hide payment block
  public deliveryAttention: boolean = false; // show/hide attention if !countryShort
  public formAttention: boolean = false; // предупреждение если не заполнена вся форма
  public deliveryIncompatibilityWithCountry: boolean = false; // предупреждение если доставка недоступна в данной стране
  public paymentIncompatibilityWithCountry: boolean = false; // предупреждение если оплата недоступна в данной стране
  public emailInvalid: boolean = false; // предупреждение если email введен некорректно
  public selectedDeliveryId: number; // id выбранной доставки
  public selectedPaymentId: number; // id способа платежа
  public cart: Cart = null; // cart
  public countries: CountryShort[] = null; //  страны
  public country: CountryShort = null; //  страна кратко
  public countryShort: CountryShort = null; //  страна
  public region: Region = null; //  регион
  public regionShort: Region = null; //  регион
  public regions: Region[] = null; //  регионы
  public countryID: number = null; //  ID страны
  public currency: Currency = null; //  текущая валюта
  public delivery: Delivery = null; //  доставка
  public deliveries: Delivery[] = null; //  доставки
  public deliveryCost: number = null; //  цена доставки
  public payment: Payment = null; //  оплата
  public payments: Payment[] = null; //  оплаты
  public payPalConfig?: IPayPalConfig;
  public showSuccess: boolean;
  public amount: string = '1';
  public imagePath: string = baseProto + imagesURL + '/products/';
  public productsCost: number = 0;  //  цена продуктов
  public address: Address = null;
  public localizations: Localization[] = null; // список локализаций
  public languageString: string = 'EN';


  // private
  private cartSub: Subscription = null;
  private countrySub: Subscription = null;
  private countriesSub: Subscription = null;
  private countriesListSub: Subscription = null;
  private currencySub: Subscription = null;
  private deliverySub: Subscription = null;
  private deliveriesSub: Subscription = null;
  private deliveryCostSub: Subscription = null;
  private paymentSub: Subscription = null;
  private paymentsSub: Subscription = null;
  private regionSub: Subscription = null;
  private regionsSub: Subscription = null;
  private regionsListSub: Subscription = null;
  private localizationsSub: Subscription; //  подписка на список локализаций
  private languageSub: Subscription;

  simpleItems = [];

  // ViewChild
  @ViewChild('paymentBlock', {static: false}) paymentBlock: ElementRef<any>; // payment block element
  @ViewChild('emailInput', {static: false}) emailInput: ElementRef<any>; // поле для ввода почты
  @ViewChildren('productCountInput') productCountInputs: QueryList<ElementRef>;  // поля для ввода количества товара

  constructor(private cartService: CartService, private dataService: DataService, private changeDetectorRef: ChangeDetectorRef,
              private paymentService: PaymentService, private currencyService: CurrencyService) {
    // subscription
    //  подписка cart
    this.cartSub = this.dataService.castCart.subscribe((cart: Cart) => {
      if (cart) {
        console.log('new.cart');
        this.cart = cart;
        //  если корзина не пустая обновляем стоимость товаров
        if (cart.cartItems && cart.cartItems.length) {
          this.productsCost = this.countProducts(cart.cartItems);
        }
      }
    });
    //  текущая страна
    this.countrySub = this.dataService.castCountry.subscribe((country: Country) => {
      this.country = country;
      if (country) {
        this.chooseDeliveryId(108);
      }
    });
    //  страны
    this.countriesSub = this.dataService.castCountriesShort.subscribe((countries: CountryShort[]) => this.countries = countries);
    //  регионы
    this.regionsListSub = this.dataService.castRegions.subscribe((regions: Region[]) => this.regions = regions);
    //  текущая валюта
    this.currencySub = this.dataService.castCurrency.subscribe((currency: Currency) => {
      this.currency = currency;
      //  если корзина не пустая обновляем стоимость товаров
      if (this.cart.cartItems && this.cart.cartItems.length) {
        this.productsCost = this.countProducts(this.cart.cartItems);
      }
    });
    //  подписываемся на текущую доставку
    this.deliverySub = this.dataService.castDelivery.subscribe((delivery: Delivery) => {
      this.delivery = delivery;
      //  если доставка не пустая обновляем стоимость доставки
      // if (this.cart.cartItems && this.cart.cartItems.length && this.country) {
      //   this.deliveryCost = this.countDelivery(this.cart.cartItems, delivery, this.country);
      // }
    });
    //  подписываемся на доставки
    this.deliveriesSub = this.dataService.castDeliveries.subscribe((deliveries: Delivery[]) => this.deliveries = deliveries);
    //  подписываемся на цену доставки
    this.deliveryCostSub = this.dataService.castDeliveryCost.subscribe((deliveryCost: number) => this.deliveryCost = deliveryCost);
    //  подписываемся на текущую оплаты
    this.paymentSub = this.dataService.castPayment.subscribe((payment: Payment) => this.payment = payment);
    //  подписываемся на оплаты
    this.paymentsSub = this.dataService.castPayments.subscribe((payments: Payment[]) => this.payments = payments);
    //  регион
    this.regionSub = this.dataService.castRegion.subscribe((region: Region) => this.region = region);
    //  регионы
    this.regionsSub = this.dataService.castRegions.subscribe((regions: Region[]) => this.regions = regions);

    //  подписка localizations
    this.localizationsSub = this.dataService.castLocalizations.subscribe((localizations: Localization[]) => {
      this.localizations = localizations;
    });

    //  подписываемся на язык
    this.languageSub = this.dataService.castLanguage.subscribe((language: Language) => {
      if (language) {
        // console.log('Have changes :', language);
        this.languageString = language.language;
        console.log(this.languageString);
      }
    });
  }

  ngOnDestroy() {
    //  отписываемся
    this.cartSub.unsubscribe();
    this.countrySub.unsubscribe();
    this.countriesSub.unsubscribe();
    // this.countriesListSub.unsubscribe();
    this.deliverySub.unsubscribe();
    this.deliveriesSub.unsubscribe();
    this.deliveryCostSub.unsubscribe();
    this.paymentSub.unsubscribe();
    this.paymentsSub.unsubscribe();
    this.regionSub.unsubscribe();
    this.regionsListSub.unsubscribe();
    this.localizationsSub.unsubscribe();
  }

  ngOnInit() {
    this.address = GenerateService.generateAddress();
    // this.initConfig();
    this.simpleItems = [true, 'Two', 3];
    //  пересчет цены продуктов
    this.productsCost = this.countProducts(this.cart.cartItems);
  }

  //  получить ордер для нового заказа и сгенерировать запрос на оплату
  public createOrder() {
    //  дергаем сервис получения номера заказа
    if (this.delivery && this.payment && this.address && this.currency && this.cart.cartItems.length) {
      this.cartService.createOrder(this.delivery.id, this.payment.id, this.address, this.currency.id, this.cart.cartItems)
        .subscribe((order: Order) => {
          if (order) {
            console.log('New order is: ', order);
            this.initConfig(String(order.id));
          }
        });
    }
  }

  // add cartItem to cart
  public incCartItem(cartItemIndex: number) {
    //  дергаем сервис
    this.cartService.incCartItem(cartItemIndex);
  }

  // remove whole cart item
  public decCartItem(cartItemIndex: number) {
    //  дергаем сервис
    this.cartService.decCartItem(cartItemIndex);
  }

  // отсекает ввод букв в поле телефона
  public checkOnPhone(event) {
    const charCode = (event.which) ? event.which : event.keyCode;
    const shiftPressed = event.shiftKey;

    if ((shiftPressed && charCode === 187) || (charCode === 107)) {
        return true;
      } else if ((charCode > 95) && (charCode < 106)) {
        return true;
      } else if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
      } else {
        return true;
      }
  }

  // отсекает ввод букв в поле
  public checkOnNumbers(event, cartItemIndex) {
    const key = Number(event.key);
    // прерывать если введено не число кроме Backspace
    if ((isNaN(key) || event.key === null) && event.key !== 'Backspace') {
      event.preventDefault();
      return;
    }

  // если есть четыре символа прерывать ввод новых кроме Backspace
    if ( this.cart.cartItems[cartItemIndex].orderCount.toString().length === 4 && event.key !== 'Backspace') {
      event.preventDefault();
    }
  }

  // изменение количества товра вручную
  public changeAmountOfProduct(cartItemIndex) {

    // измененное поле ввода
    const input = this.productCountInputs.toArray()[cartItemIndex];

    // удалены все символы - подставлять ноль
    if (input.nativeElement.value.length === 0) {
      this.cart.cartItems[cartItemIndex].orderCount = 0;
      this.dataService.publishCart(this.cart);
      return;
    }

    // если введенная величина больше трех симоволов - сбрасывать до 999
    if (input.nativeElement.value.length > 3) {
      this.cart.cartItems[cartItemIndex].orderCount = 999;
      this.dataService.publishCart(this.cart);
      return;
    }

    // присвоить введенное значение
    this.cart.cartItems[cartItemIndex].orderCount = parseInt(input.nativeElement.value, 10);
    this.dataService.publishCart(this.cart);
  }

  // отсекает ввод цифр в поле
  public checkOnString(event) {
    const charCode = event.keyCode;

    if (
      (charCode > 64 && charCode < 91) ||
      (charCode > 96 && charCode < 123) ||
      charCode === 8 ||
      charCode === 'Backspace' ||
      charCode === 32) {
      return;
    } else {
      event.preventDefault();
    }
  }

  // валидация поля почты через атрибут pattern
  emailValidation() {
    if (this.emailInput.nativeElement.validity.valid) {
      this.emailInvalid = false;
      return true;
    } else {
      this.emailInvalid = true;
      return false;
    }
  }


  //  генерация пути
  public getImagePath(image: Image): string {
    return baseProto + imagesURL + '/' + image.type.path + '/' + image.url + '.jpg';
  }

  //  выбрали страну
  public chooseCountry() {
    //  получаем все регионы
    this.cartService.getRegionsByCountryID(this.countryShort.id);
    //  получаем текущую страну в полном формате
    this.cartService.getCountryByID(this.countryShort.id);
  }

  //  выбрали страну
  public chooseRegion() {
    //  получаем текущий регион в полном формате
    this.cartService.getRegionByID(this.countryShort.id);
  }

  //  выбрать доставку
  public chooseDeliveryId(deliveryId: number) {
    // показать предупреждение если доставка недоступна для данной страны
    if (deliveryId === 106 || deliveryId === 107) {
      this.deliveryIncompatibilityWithCountry = true;
      this.selectedDeliveryId = null;
      return;
    } else {
      this.deliveryIncompatibilityWithCountry = false;
    }


    //  получаем экземпляр оплаты
    this.cartService.getDeliveryByID(deliveryId);
    this.selectedDeliveryId = deliveryId;
    this.cart.deliveryID = deliveryId;
    this.cart.address = this.address;
    const country: Country = {
      id: this.countryShort.id,
      localizedDescriptions: []
    };
    this.cart.address.country = country;
    this.cartService.publishCart(this.cart);
    //  дернули сервис получения цены доставки
    console.log('getDeliveryCost cart:', this.cart);
    this.cartService.getDeliveryCost(this.cart);
  }

  //  выбрать способ оплаты
  public choosePaymentId(paymentId: number) {
    // показать предупреждение если оплата недоступна для данной страны
    if (paymentId === 3) {
      this.paymentIncompatibilityWithCountry = true;
      this.selectedPaymentId = null;
      return;
    } else {
      this.paymentIncompatibilityWithCountry = false;
    }

    console.log('change payment to: ', paymentId);
    //  получаем экземпляр оплаты
    this.cartService.getPaymentByID(paymentId);
    this.selectedPaymentId = paymentId;
  }

  //  посчитать доставку
  // public countDelivery(cartItems: CartItem[], delivery: Delivery, country: CountryShort): number {
  //   let totalWeight: number = 0;
  //   //  какой тип веса
  //   if (delivery.weightType) {
  //     //  regular
  //     totalWeight = this.getProductsWeight(cartItems);
  //   } else {
  //     //  volume
  //     totalWeight = this.getProductsVolumeWeight(cartItems);
  //   }
  //   //  возвращаем посчитанную доставку
  //   return this.cartService.getDeliveryCosts(country.id, totalWeight);
  // }

  //  посчитать товары
  public countProducts(cartItems: CartItem[]): number {
    let productsCost: number = 0;
    //  если есть товары
    if (cartItems && cartItems.length) {
      //  перебираем айтемы
      cartItems.forEach((cartItem: CartItem) => {
        //  прибавляем произведение количества на стоимость одного к общей сумме
        productsCost += this.currencyService.convertToCurrency(cartItem.product.price.sell) * cartItem.orderCount;
      });
    }
    return productsCost;
  }

  // show / hide comment
  public invertShowComment() {
    this.showComment = !this.showComment;
  }

  // show payment block
  public moveToPayment() {
    // валидация email
    this.emailValidation();
    // не давать переходить к оплате пока не заполнены все поля
    if (
      !this.address.firstName ||
      !this.address.lastName ||
      !this.address.phone ||
      !this.address.email ||
      !this.address.description ||
      !this.address.city ||
      !this.address.zip ||
      !this.countryShort ||
      !this.selectedDeliveryId ||
      !this.selectedPaymentId ||
      this.emailInvalid
    ) {
      this.formAttention = true;
      return;
    }

    this.formAttention = false;
    this.showPaymentBlock = true;
    this.changeDetectorRef.detectChanges();
    this.paymentBlock.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center', inline: 'start' });
    this.createOrder();
  }

  // предупреждение о необходимости выбрать страну
  public showDeliveryAttention() {
    this.deliveryAttention = true;
  }

  //  конфигуратор для paypal
  private initConfig(orderID: string): void {
    //  переменные
    const purchaseUnits: IPurchaseUnit[] = []; //  все блоки покупок
    let items: ITransactionItem[] = [];
    //  заполняем товарные позиции
    if (this.cart && this.cart.cartItems) {
      items = this.cart.cartItems.map((cartItem: CartItem) => {
        const newItem: ITransactionItem = {
          name: cartItem.product.localizedDescriptions.find(locale => locale.language === 'EN').name,
          quantity: String(cartItem.orderCount),  //  количество
          unit_amount: {
            currency_code: this.currency.code,
            value: String(this.currencyService.convertToCurrency(cartItem.product.price.sell)),
          }
        };
        return newItem;
      });
    }
    //  добавляем доставку
    items.push(this.paypalDeliveryCount());
    //  формируем итоговый модуль
    const purchaseUnit: IPurchaseUnit = {
      amount: this.paypalTotalCount(),
      items,
      invoice_id: orderID
    }; //  один блок
    //  пушим юнита
    purchaseUnits.push(purchaseUnit);

    console.log('initConf:', purchaseUnits);

    this.payPalConfig = {
      currency: this.currency.code,
      clientId: 'AXc_EI6-eB-7LEHLQ0uBujO6LbIFT7srHv9SNxTiW2AdLMCr7PmXLhgf1UQA9lKDCfNFOHFZeczATsVy',
      // clientId: 'AXc_EI6-eB-7LEHLQ0uBujO6LbIFT7srHv9SNxTiW2AdLMCr7PmXLhgf1UQA9lKDCfNFOHFZeczATsVy',
      // tslint:disable-next-line
      createOrderOnClient: (data) => <ICreateOrderRequest> {
        intent: 'CAPTURE',
        purchase_units: purchaseUnits
        },
      advanced: {
        commit: 'true'
      },
      style: {
        label: 'paypal',
        layout: 'vertical',
        size: 'large',
        // shape: 'pill',
        color: 'silver',
        // tagline: false
      },
      onApprove: (data, actions) => {
        console.log('onApprove - transaction was approved, but not authorized', data, actions);
        actions.order.get().then(details => {
          console.log('onApprove - you can get full order details inside onApprove: ', details);
        });
      },
      onClientAuthorization: (data) => {
        console.log('onClientAuthorization - you should probably inform your server about completed transaction at this point', data);
        const myData: any = data;
        // this.paymentService.checkPayment(myData.purchase_units[0].payments.captures[0].id, Number(this.amount), this.currency.code);
        this.showSuccess = true;
      },
      onCancel: (data, actions) => {
        console.log('OnCancel', data, actions);
      },
      onError: err => {
        console.log('OnError', err);
      },
      onClick: (data, actions) => {
        console.log('onClick', data, actions);
      },
    };
  }

  //  посчитать суммарный вес товара
  private getProductsWeight(cartItems: CartItem[]): number {
    return 0;
  }

  //  посчитать суммарный объемный вес товара
  private getProductsVolumeWeight(cartItems: CartItem[]): number {
    return 0;
  }

  //  paypal
  //  сумма всего
  private paypalTotalCount(): IUnitAmount {
    return {
      currency_code: this.currency.code,
      value: String(this.productsCost + this.currencyService.convertToCurrency(this.deliveryCost)),
      breakdown: {
        item_total: {
          currency_code: this.currency.code,
          value: String(this.productsCost + this.currencyService.convertToCurrency(this.deliveryCost)),
        }
      }
    };
  }

  //  цена одной
  private paypalDeliveryCount(): ITransactionItem {
    return {
      name: 'Shipping',
      quantity: '1',
      unit_amount: {
        currency_code: this.currency.code,
        value: String(this.currencyService.convertToCurrency(this.deliveryCost)),
      }
    };
  }
}
