import {Component, OnInit} from '@angular/core';
import {Animations} from '../animations';
import {GeneralAnimations} from './general.animations';
import {ICreateOrderRequest, IPayPalConfig} from 'ngx-paypal';


@Component({
  selector: 'app-home',
  animations: [Animations.fadeInOut, GeneralAnimations.arrowUpDown],
  templateUrl: './general.component.html',
  styleUrls: ['./general.component.scss']
})
export class GeneralComponent implements OnInit {
  // public
  public showImage: boolean = true; // show/hide image

  constructor() {
  }

  ngOnInit() {
    // this.showPayment = false;
  }

  // show/hide image
  public invertShowImage() {
    this.showImage = !this.showImage;
  }
}
