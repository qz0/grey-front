// animations.ts
import { trigger, state, style, transition, animate } from '@angular/animations';

export const GeneralAnimations = {
  // Вращение кнопки сворачивания
  arrowUpDown: trigger('arrowUpDown', [
    state('up', style({
      position: 'absolute',
      transform: 'translate(-50%, -105%) rotate(0deg)',
      top: '105%',
      left: '50%'
    })),
    state('down', style({
      position: 'absolute',
      transform: 'translate(-50%, 50%) rotate(180deg)',
      top: '0',
      left: '50%',
      'z-index': '1'
    })),
    transition('up <=> down', [
      animate('.2s')
    ])
  ])
};
