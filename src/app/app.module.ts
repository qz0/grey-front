import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GeneralComponent } from './general/general.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import {MenuComponent} from './components/menu/menu.component';
import {ProductsModule} from './products/products.module';
import {CartModule} from './cart/cart.module';
import { FooterComponent } from './components/footer/footer.component';
import { ContactComponent } from './components/contact/contact.component';
import {BrockerModule} from './brocker/brocker.module';
import {PipesModule} from './_pipes/pipes.module';
import {PaymentModule} from './payment/payment.module';
import {NgxPayPalModule} from 'ngx-paypal';
import {NgSelectModule} from '@ng-select/ng-select';
import {DirectivesModule} from './__directives/directives.module';
import { PrivacyComponent } from './components/privacy/privacy.component';
import { OfferComponent } from './components/offer/offer.component';
import { DeliveryTypesComponent } from './components/delivery-types/delivery-types.component';
import { NgxCaptchaModule } from 'ngx-captcha';

@NgModule({
  declarations: [
    AppComponent,
    GeneralComponent,
    MenuComponent,
    NotFoundComponent,
    FooterComponent,
    ContactComponent,
    PrivacyComponent,
    OfferComponent,
    DeliveryTypesComponent,
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ProductsModule,
    CartModule,
    FormsModule,
    BrockerModule,
    PipesModule,
    PaymentModule,
    NgxPayPalModule,
    NgSelectModule,
    DirectivesModule,
    NgxCaptchaModule,
    ReactiveFormsModule
  ],
  providers: [],
  exports: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
