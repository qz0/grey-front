import {Directive, ElementRef, HostListener, AfterContentChecked} from '@angular/core';

@Directive({
  selector: '[appAutowidth]'
})
export class InputAutoWidthDirective implements AfterContentChecked {

  constructor(private el: ElementRef) {}

  @HostListener('keyup') onKeyUp() {
    this.resize();
  }

  @HostListener('focus') onFocus() {
    this.resize();
  }

  ngAfterContentChecked(): void {
    this.resize();
  }

  private resize() {
    if (this.el.nativeElement.value.length === 0) {
      this.el.nativeElement.style.width = '1rem';
    } else {
      this.el.nativeElement.style.width = ((this.el.nativeElement.value.length + 1) * 8) + 'px';
    }
  }
}
