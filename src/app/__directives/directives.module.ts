import { NgModule } from '@angular/core';
import { AngularFittextDirective } from './fittext';
import {InputAutoWidthDirective} from './inputAutoWidth';

@NgModule({
    imports: [],
    declarations: [AngularFittextDirective, InputAutoWidthDirective],
    exports: [AngularFittextDirective, InputAutoWidthDirective]
})
export class DirectivesModule { }