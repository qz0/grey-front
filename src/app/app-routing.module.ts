import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {NotFoundComponent} from './components/not-found/not-found.component';
import {GeneralComponent} from './general/general.component';
import {productsRoutes} from './products/products-routing.module';
import {CheckOutComponent} from './cart/check-out/check-out.component';
import {ContactComponent} from './components/contact/contact.component';
import {brockerRoutes} from './brocker/brocker-routing.module';
import {paymentsRoutes} from './payment/payment-routing.module';
import {PaymentComponent} from './payment/payment/payment.component';
import { PrivacyComponent } from './components/privacy/privacy.component';
import { OfferComponent } from './components/offer/offer.component';
import { DeliveryTypesComponent } from './components/delivery-types/delivery-types.component';
import {urlSberbank} from './_models/urls';

const routes: Routes = [
  //  домашняя
  { path: '', component: GeneralComponent, pathMatch: 'full' },
  //  покупка
  { path: 'checkout', component: CheckOutComponent },
  //  оплаты
  {
    path: 'payments',
    children: paymentsRoutes,
    // component: PaymentComponent,
  },
  //  продукты
  {
    path: 'products',
    children: productsRoutes,
  },
  // контакты
  { path: 'contact', component: ContactComponent},
  //  not found
  { path: 'not_found', component: NotFoundComponent },
  // Политика конфидициональности
  { path: 'privacy', component: PrivacyComponent },
  // ПУБЛИЧНАЯ ОФЕРТА
  { path: 'offer', component: OfferComponent },
  // Информация о доставке
  { path: 'delivery-types', component: DeliveryTypesComponent },
  //  путь по умолчанию
  // { path: '**', redirectTo: '/not_found', pathMatch: 'full' },
  // {
  //   path: '**/:seoLink',
  //   children: brockerRoutes,
  // },
  {
    path: '**',
    children: brockerRoutes,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
