import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {PaymentComponent} from './payment/payment.component';
import {FinalComponent} from './final/final.component';


export const paymentsRoutes: Routes = [
  //  домашняя
  {
    path: '', component: PaymentComponent
  },
  {
    path: 'payment', component: PaymentComponent
  },
  {
    path: 'final', component: FinalComponent
  },
  ];

@NgModule({
  imports: [
    RouterModule.forChild(paymentsRoutes)
  ],
  exports: [RouterModule]
})
export class PaymentRoutingModule { }
