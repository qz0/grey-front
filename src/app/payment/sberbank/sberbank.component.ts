import {Component, OnDestroy, OnInit} from '@angular/core';
import { Router } from '@angular/router';
import {Subscription} from 'rxjs';
import {DataService} from '../../_services/data.service';

import {SberbankRegisterRequest} from '../../_models/sberbank.types';

@Component({
  selector: 'app-sberbank',
  templateUrl: './sberbank.component.html',
  styleUrls: ['./sberbank.component.scss']
})
export class SberbankComponent implements OnInit, OnDestroy {
  public paymentURL: string = null; //  url перехода на страницу оплаты

  //  private
  private paymentUrlSub: Subscription;  //  подписка на url оплаты

  constructor(private router: Router, private dataService: DataService) {

    //  подписка paymentURL
    this.paymentUrlSub = this.dataService.castPaymentURL.subscribe((paymentURL: string) => {
      console.log('paymentURL: ', paymentURL);
      this.paymentURL = paymentURL;
    });
  }

  ngOnDestroy() {
    //  unsubscribe
    this.paymentUrlSub.unsubscribe();
  }

  ngOnInit() {
  }

  // показать окно оплаты
  public showPayWindow() {
    this.router.navigate(['/'])
      .then(() => {
        window.location.href = this.paymentURL;
      });
  }
}

