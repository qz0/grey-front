import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SberbankComponent } from './paypal.component';

describe('SberbankComponent', () => {
  let component: SberbankComponent;
  let fixture: ComponentFixture<SberbankComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SberbankComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SberbankComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
