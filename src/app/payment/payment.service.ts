import { Injectable } from '@angular/core';
import {DataService} from '../_services/data.service';
import {Product} from '../_models/product.types';
import {OptionValue} from '../_models/option.types';
import {Cart, CartServer} from '../_models/cart.types';
import {baseProto, baseUrl, urlCart} from '../_models/urls';
import {catchError, map, tap} from 'rxjs/operators';
import {BehaviorSubject, Observable, of} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {TinkoffInitResponse, TinkoffPaymentRequest} from '../_models/payment.types';
import {Address} from '../_models/address.types';

@Injectable({
  providedIn: 'root'
})
export class PaymentService {
  //  public
  // public paymentURL: string = null; //  link для оплаты от тинькова

  //  cast
  //  cast paymentURL
  public castPaymentURL: Observable<string>;

  //  private
  //  bs
  //  bs for paymentURL
  private paymentURL: BehaviorSubject<string> = new BehaviorSubject<string>(null);

  constructor(private dataService: DataService, private http: HttpClient) {
    //  link casts & bs for
    //  link casts & bs for address
    this.castPaymentURL = this.paymentURL.asObservable();
  }

  //  добавить продукт в корзину
  public initTinkoffPayment() {
    const request: TinkoffPaymentRequest = {
      TerminalKey: '1576850526628DEMO',
      Amount: '10000',
      OrderId: '21050',
      Description: 'Подарочная карта на 1400.00 рублей',
      DATA: {
        Email: 'a@test.com',
        Phone: '+71234567890',
      },
      Receipt: {
        Email: 'a@test.ru',
        Phone: '+79031234567',
        EmailCompany: 'b@test.ru',
        Taxation: 'osn',
        Items: [{
          Name: 'Наименование товара 1',
          Price: 10000,
          Quantity: 1.00,
          Amount: 10000,
          Tax: 'vat10'
        }]
      }
    };

    this.http.post<TinkoffInitResponse>('https://securepay.tinkoff.ru/v2/Init', request)
      .pipe(
        // map(re => this.convertService.cartServerToClient(cartServer)),
        tap(response => {
          //  если прилетел URL - публикуем
          if (response && response.PaymentURL) {
            console.log('TinkoffInitResponse: ', response.PaymentURL);
            this.paymentURL.next(response.PaymentURL);
          }
        }),
        catchError(err => {
          console.log(err, 'ERROR in createCart service');
          return of(null);
        })
      )
    .subscribe();
  }

  //  дергаем сервис проверки статуса
  public paypalCheckStatus(token: string) {
    if (token) {
      this.dataService.paypalCheckStatus(token);
    }
  }

  //  дергаем сервис проверки статуса
  public sberbankCheckStatus(orderID: string) {
    if (orderID) {
      this.dataService.sberbankCheckStatus(orderID);
    }
  }
}
