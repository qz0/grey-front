import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {DataService} from '../../_services/data.service';
import {PaymentService} from '../payment.service';
import {Subscription} from 'rxjs';
import { Order } from 'src/app/_models/order.types';
import {Product} from '../../_models/product.types';
import {Language} from '../../_models/localization.types';

@Component({
  selector: 'app-final',
  templateUrl: './final.component.html',
  styleUrls: ['./final.component.scss']
})
export class FinalComponent implements OnInit, OnDestroy {
  //  public
  public order: Order = null; //  заказ
  public orderParams = null; // параметры заказа
  public languageString: string = 'EN';

  //  private
  private orderSub: Subscription;
  private languageSub: Subscription;

  constructor(private activatedRoute: ActivatedRoute, private dataService: DataService, private paymentService: PaymentService) {
    //  подписываемся на асинхроны
    this.orderSub = this.dataService.castOrder.subscribe((order: Order) => {
      if (order) {
        this.order = order;
        console.log('ORDER: ', this.order);
      }
    });

    //  подписываемся на язык
    this.languageSub = this.dataService.castLanguage.subscribe((language: Language) => {
      if (language) {
        // console.log('Have changes :', language);
        this.languageString = language.language;
        console.log(this.languageString);
      }
    });
  }

  //  дестроер
  ngOnDestroy() {
    //  отписываемся от подписок
    this.orderSub.unsubscribe();
  }

  //  инит
  ngOnInit() {
    // let orderID: string;
    //  получаем id
    this.activatedRoute.queryParams.subscribe((params: {orderId: string, lang: string, token: string}) => {
      // присвоить параметры заказа
      if (params) {
        this.orderParams = params;

        //  если не пустой - дергаем сервис подтверждения
        //  если orderId - это сбер
        if (params.orderId) {
          this.paymentService.sberbankCheckStatus(params.orderId);
        }

        //  если token - это paypal
        if (params.token) {
          this.paymentService.paypalCheckStatus(params.token);
        }
      }
    });


  }

}
