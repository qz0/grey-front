import {Component, OnDestroy, OnInit} from '@angular/core';
import {ICreateOrderRequest, IPayPalConfig} from 'ngx-paypal';
import {PaymentServer} from '../../_models/payment.types';
import {PaymentService} from '../payment.service';
import {Category} from '../../_models/category.types';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss']
})
export class PaymentComponent implements OnInit, OnDestroy {
  //
  public payPalConfig?: IPayPalConfig;
  public showSuccess: boolean;
  public amount: string = '';
  public paymentURL: string = null; //  url перехода на страницу оплаты

  //  private
  private paymentUrlSub: Subscription;  //  подписка на url оплаты


  constructor(private paymentService: PaymentService) {

    //  подписка paymentURL
    this.paymentUrlSub = this.paymentService.castPaymentURL.subscribe((paymentURL: string) => {
      this.paymentURL = paymentURL;
    });
  }

  ngOnDestroy() {
    //  unsubscribe
    this.paymentUrlSub.unsubscribe();
  }

  ngOnInit() {
  }

  public initPayment() {
    this.paymentService.initTinkoffPayment();
  }

}
