import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PaymentRoutingModule } from './payment-routing.module';
import { PaymentComponent } from './payment/payment.component';
import {RouterModule} from '@angular/router';
import {NgxPayPalModule} from 'ngx-paypal';
import {FormsModule} from '@angular/forms';
import {PipesModule} from '../_pipes/pipes.module';
import { TinkoffComponent } from './tinkoff/tinkoff.component';
import { SberbankComponent } from './sberbank/sberbank.component';
import { FinalComponent } from './final/final.component';
import {PaypalComponent} from './paypal/paypal.component';


@NgModule({
  declarations: [
    PaymentComponent,
    PaypalComponent,
    TinkoffComponent,
    SberbankComponent,
    FinalComponent
  ],
  imports: [
    CommonModule,
    PaymentRoutingModule,
    RouterModule,
    NgxPayPalModule,
    FormsModule,
    PipesModule,
  ],
  exports: [
    PaymentComponent,
    PaypalComponent,
    SberbankComponent,
    TinkoffComponent
  ]
})
export class PaymentModule { }
