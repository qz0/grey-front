import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TinkoffComponent } from './tinkoff.component';

describe('TinkoffComponent', () => {
  let component: TinkoffComponent;
  let fixture: ComponentFixture<TinkoffComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TinkoffComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TinkoffComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
