import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {PaymentService} from '../payment.service';

@Component({
  selector: 'app-tinkoff',
  templateUrl: './tinkoff.component.html',
  styleUrls: ['./tinkoff.component.scss']
})
export class TinkoffComponent implements OnInit, OnDestroy {
  //  public
  public paymentURL: string = null; //  url перехода на страницу оплаты
  public payModal: boolean = false; // модальное окно оплаты

  //  private
  private paymentUrlSub: Subscription;  //  подписка на url оплаты

  constructor(private paymentService: PaymentService) {
    //  подписка paymentURL
    this.paymentUrlSub = this.paymentService.castPaymentURL.subscribe((paymentURL: string) => {
      this.paymentURL = paymentURL;
    });
  }

  ngOnInit() {
    //  initim tinkoff
    this.paymentService.initTinkoffPayment();
  }

  ngOnDestroy() {
    //  unsubscribe
    this.paymentUrlSub.unsubscribe();
  }

  // показать модальное окно оплаты
  public showPayModal() {
    this.payModal = !this.payModal;
  }

}
