import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BrockerComponent } from './brocker.component';

describe('BrockerComponent', () => {
  let component: BrockerComponent;
  let fixture: ComponentFixture<BrockerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BrockerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BrockerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
