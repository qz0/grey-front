import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {BrockerService} from '../brocker.service';
import {Category} from '../../_models/category.types';
import {Product} from '../../_models/product.types';

@Component({
  selector: 'app-brocker',
  templateUrl: './brocker.component.html',
  styleUrls: ['./brocker.component.scss']
})
export class BrockerComponent implements OnInit {

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private brockerService: BrockerService) { }

  ngOnInit() {
    //  получаем шв
    // const seoLink = this.activatedRoute.snapshot.paramMap.get('seoLink');
    // console.log('Product id from RouterActivated: ', productID);
    const seoLink = String(this.router.url).slice(1);
    console.log('BrockerComponent - OnInit - seoLink: ', seoLink);
    this.brockerService.getContent(seoLink).subscribe((data: Category|Product|any) => {
      // console.log('Type of data: ', typeof data);
      if (data.type === 'category') {
        const category: Category = data;
        console.log('BrockerComponent - OnInit - category: ', category);
        this.router.navigate(['/products/category/', String(category.id)], { skipLocationChange: true });
      } else if (data.type === 'product') {
        const product: Product = data;
        this.router.navigate(['/products/product/', String(product.id)], { skipLocationChange: true });
        console.log('PRODUCT!!!!');
      } else {
        console.log('UNKNOWN TYPE!!!!');
      }
    });
    // this.router.navigate(['/brocker/read_content?seoLink=' + stringForSearch], { skipLocationChange: true });
  }

}
