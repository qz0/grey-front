import { Injectable } from '@angular/core';
import {DataService} from '../_services/data.service';
import {Observable, of} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BrockerService {

  constructor(private dataService: DataService) { }

  //  получение данных
  public getContent(seoLink: string): Observable<string> {
    if (seoLink && seoLink.length) {
      return this.dataService.getContent(seoLink);
    } else {
      return of(null);
    }
  }
}
