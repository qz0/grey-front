import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {BrockerComponent} from './brocker/brocker.component';

export const brockerRoutes: Routes = [
  {
    //  все products
    path: ':seoLink', component: BrockerComponent
  },
  {
    //  все products
    path: '', component: BrockerComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(brockerRoutes)],
  exports: [RouterModule]
})
export class BrockerRoutingModule { }
