import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BrockerRoutingModule } from './brocker-routing.module';
import { BrockerComponent } from './brocker/brocker.component';


@NgModule({
  declarations: [
    BrockerComponent
  ],
  imports: [
    CommonModule,
    BrockerRoutingModule
  ]
})
export class BrockerModule { }
