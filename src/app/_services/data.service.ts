import { Injectable} from '@angular/core';
import {BehaviorSubject, Observable, of} from 'rxjs';
import {Option, OptionsSet, OptionType, OptionValue} from '../_models/option.types';
import {Menu} from '../_models/menu.types';
import {Brand, Manufacturer} from '../_models/brand.types';
import {Language, LanguageServer, Localization, LocalizationServer} from '../_models/localization.types';
import {Image} from '../_models/image.types';
import {Delivery, DeliveryServer} from '../_models/delivery.types';
import {Address, Country, CountryServer, CountryShort, CountryShortServer, Region, RegionServer} from '../_models/address.types';
import {Category, CategoryServer} from '../_models/category.types';
import {Product, ProductServer, ProductShadow} from '../_models/product.types';
import {Order, OrderPriority, OrderServer, OrderStatus} from '../_models/order.types';
import {Payment, PaymentServer} from '../_models/payment.types';
import {Currency, CurrencyServer} from '../_models/currency.types';
import {Customer, CustomerRank} from '../_models/customer.types';
import {Item, ItemShadow} from '../_models/item.types';
import {ConvertService} from './convert.service';
import {HttpClient, HttpParams} from '@angular/common/http';
import {GenerateService} from './generate.service';
import {
  baseProto,
  baseUrl, urlBrocker,
  urlCart, urlCategory,
  urlCountry,
  urlCurrency,
  urlDelivery,
  urlLanguage, urlLocalization, urlOrder,
  urlPayment, urlPaypal,
  urlProduct, urlRegion, urlSberbank
} from '../_models/urls';
import {catchError, map, tap} from 'rxjs/operators';
import {Cart, CartItem, CartServer} from '../_models/cart.types';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  //  public
  //  casts
  //  cast assembly
  // public castAssembly: Observable<Assembly>;
  // //  cast assemblies
  // public castAssemblies: Observable<Assembly[]>;
  //  cast address
  public castAddress: Observable<Address>;
  //  cast addresses
  public castAddresses: Observable<Address[]>;
  //  cast brand
  public castBrand: Observable<Brand>;
  //  cast brands
  public castBrands: Observable<Brand[]>;
  //  cast cart
  public castCart: Observable<Cart>;
  //  cast carts
  public castCarts: Observable<Cart[]>;
  //  cast cart is shown
  public castCartIsShownBit: Observable<boolean>;
  //  cast cart
  public castCartItem: Observable<CartItem>;
  //  cast carts
  public castCartItems: Observable<CartItem[]>;
  //  cast category
  public castCategory: Observable<Category>;
  //  cast categoriesAll
  public castCategoriesAll: Observable<Category[]>;
  //  cast categoriesTop
  public castCategoriesTop: Observable<Category[]>;
  //  cast countries
  public castCountry: Observable<Country>;
  //  cast countries
  public castCountries: Observable<Country[]>;
  //  cast countriesShort
  public castCountriesShort: Observable<CountryShort[]>;
  //  cast countriesList
  public castCountriesList: Observable<any[]>;
  //  cast customer
  public castCustomer: Observable<Customer>;
  //  cast customers
  public castCustomers: Observable<Customer[]>;
  //  cast customer rank
  public castCustomerRank: Observable<CustomerRank>;
  //  cast customer ranks
  public castCustomerRanks: Observable<CustomerRank[]>;
  //  cast currency
  public castCurrency: Observable<Currency>;
  //  cast currencies
  public castCurrencies: Observable<Currency[]>;
  //  cast delivery
  public castDelivery: Observable<Delivery>;
  //  cast deliveries
  public castDeliveries: Observable<Delivery[]>;
  //  cast delivery cost
  public castDeliveryCost: Observable<number>;
  // //  cast incoming
  // public castIncoming: Observable<Incoming>;
  // //  cast incomings
  // public castIncomings: Observable<Incoming[]>;
  //  cast image
  public castImage: Observable<Image>;
  //  cast images
  public castImages: Observable<Image[]>;
  // //  cast item
  public castItem: Observable<Item>;
  //  cast items
  public castItems: Observable<Item[]>;
  //  cast item shadow
  public castItemShadow: Observable<ItemShadow>;
  //  cast item shadows
  public castItemShadows: Observable<ItemShadow[]>;
  //  cast localizations
  public castLocalizations: Observable<Localization[]>;
  //  cast language
  public castLanguage: Observable<Language>;
  //  cast languages
  public castLanguages: Observable<Language[]>;
  //  cast manufacturer
  public castManufacturer: Observable<Manufacturer>;
  //  cast manufacturers
  public castManufacturers: Observable<Manufacturer[]>;
  //  cast admin-menu
  public castMenu: Observable<Menu[]>;
  //  cast option
  public castOption: Observable<Option>;
  //  cast options
  public castOptions: Observable<Option[]>;
  //  cast options set
  public castOptionsSet: Observable<OptionsSet>;
  //  cast options sets
  public castOptionsSets: Observable<OptionsSet[]>;
  //  cast option types
  public castOptionTypes: Observable<OptionType[]>;
  //  cast option value
  public castOptionValue: Observable<OptionValue>;
  //  cast option values
  public castOptionValues: Observable<OptionValue[]>;
  //  cast order
  public castOrder: Observable<Order>;
  //  cast orders
  public castOrders: Observable<Order[]>;
  //  cast order priorities
  public castOrderPriorities: Observable<OrderPriority[]>;
  //  cast order statuses
  public castOrderStatuses: Observable<OrderStatus[]>;
  //  cast payment
  public castPayment: Observable<Payment>;
  //  cast payments
  public castPayments: Observable<Payment[]>;
  //  cast payments
  public castPaymentState: Observable<string>;
  //  cast paymentURL
  public castPaymentURL: Observable<string>;
  //  cast product
  public castProduct: Observable<Product>;
  //  cast products
  public castProducts: Observable<Product[]>;
  //  cast product shadow
  public castProductShadow: Observable<ProductShadow>;
  //  cast product shadows
  public castProductShadows: Observable<ProductShadow[]>;
  //  cast purchase
  // public castPurchase: Observable<Purchase>;
  // //  cast purchases
  // public castPurchases: Observable<Purchase[]>;
  //  cast region
  public castRegion: Observable<Region>;
  //  cast regions
  public castRegions: Observable<Region[]>;
  //  cast show category picture
  public castShowCategoryPicture: Observable<boolean>;

  //  private
  //  bs
  //  bs for addresses
  private address: BehaviorSubject<Address> = new BehaviorSubject<Address>(null);
  //  bs for addresses
  private addresses: BehaviorSubject<Address[]> = new BehaviorSubject<Address[]>(null);
  //  bs for assembly
  // private assembly: BehaviorSubject<Assembly> = new BehaviorSubject<Assembly>(null);
  // //  bs for assemblies
  // private assemblies: BehaviorSubject<Assembly[]> = new BehaviorSubject<Assembly[]>(null);
  //  bs for brand
  private brand: BehaviorSubject<Brand> = new BehaviorSubject<Brand>(null);
  //  bs for brands
  private brands: BehaviorSubject<Brand[]> = new BehaviorSubject<Brand[]>(null);
  //  bs for cart
  private cart: BehaviorSubject<Cart> = new BehaviorSubject<Cart>(null);
  //  bs for carts
  private carts: BehaviorSubject<Cart[]> = new BehaviorSubject<Cart[]>(null);
  //  bs for cart Is Shown Bit
  private cartIsShownBit: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  //  bs for cart
  private cartItem: BehaviorSubject<CartItem> = new BehaviorSubject<CartItem>(null);
  //  bs for carts
  private cartItems: BehaviorSubject<CartItem[]> = new BehaviorSubject<CartItem[]>(null);
  //  bs for category
  private category: BehaviorSubject<Category> = new BehaviorSubject<Category>(null);
  //  bs for categoriesAll
  private categoriesAll: BehaviorSubject<Category[]> = new BehaviorSubject<Category[]>(null);
  //  bs for categoriesTop
  private categoriesTop: BehaviorSubject<Category[]> = new BehaviorSubject<Category[]>(null);
  //  bs for countries
  private country: BehaviorSubject<Country> = new BehaviorSubject<Country>(null);
  //  bs for countries Select2OptionData
  private countries: BehaviorSubject<Country[]> = new BehaviorSubject<Country[]>(null);
  //  bs for countriesShort
  private countriesShort: BehaviorSubject<CountryShort[]> = new BehaviorSubject<CountryShort[]>(null);
  //  bs for countriesList
  private countriesList: BehaviorSubject<any[]> = new BehaviorSubject<any[]>(null);
  //  bs for currency
  private currency: BehaviorSubject<Currency> = new BehaviorSubject<Currency>(
    {id: 0, code: '', multiplier: 1, title: '', symbol: '', before: true});
  //  bs for currencies
  private currencies: BehaviorSubject<Currency[]> = new BehaviorSubject<Currency[]>(null);
  //  bs for customer
  private customer: BehaviorSubject<Customer> = new BehaviorSubject<Customer>(null);
  //  bs for customers
  private customers: BehaviorSubject<Customer[]> = new BehaviorSubject<Customer[]>(null);
  //  bs for customer rank
  private customerRank: BehaviorSubject<CustomerRank> = new BehaviorSubject<CustomerRank>(null);
  //  bs for customer ranks
  private customerRanks: BehaviorSubject<CustomerRank[]> = new BehaviorSubject<CustomerRank[]>(null);
  //  bs for delivery
  private delivery: BehaviorSubject<Delivery> = new BehaviorSubject<Delivery>(null);
  //  bs for deliveries
  private deliveries: BehaviorSubject<Delivery[]> = new BehaviorSubject<Delivery[]>(null);
  //  bs for deliveries
  private deliveryCost: BehaviorSubject<number> = new BehaviorSubject<number>(0);
  //  bs for incoming
  // private incoming: BehaviorSubject<Incoming> = new BehaviorSubject<Incoming>(null);
  // //  bs for incomings
  // private incomings: BehaviorSubject<Incoming[]> = new BehaviorSubject<Incoming[]>(null);
  //  bs for image
  private image: BehaviorSubject<Image> = new BehaviorSubject<Image>(null);
  //  bs for images
  private images: BehaviorSubject<Image[]> = new BehaviorSubject<Image[]>(null);
  //  bs for item
  private item: BehaviorSubject<Item> = new BehaviorSubject<Item>(null);
  //  bs for items
  private items: BehaviorSubject<Item[]> = new BehaviorSubject<Item[]>(null);
  //  bs for item shadow
  private itemShadow: BehaviorSubject<ItemShadow> = new BehaviorSubject<ItemShadow>(null);
  //  bs for item shadows
  private itemShadows: BehaviorSubject<ItemShadow[]> = new BehaviorSubject<ItemShadow[]>(null);
  //  bs for localizations
  private localizations: BehaviorSubject<Localization[]> = new BehaviorSubject<Localization[]>(null);
  //  bs for language
  private language: BehaviorSubject<Language> = new BehaviorSubject<Language>(null);
  //  bs for languages
  private languages: BehaviorSubject<Language[]> = new BehaviorSubject<Language[]>(null);
  //  bs for manufacturer
  private manufacturer: BehaviorSubject<Manufacturer> = new BehaviorSubject<Manufacturer>(null);
  //  bs for manufacturers
  private manufacturers: BehaviorSubject<Manufacturer[]> = new BehaviorSubject<Manufacturer[]>(null);
  //  bs for admin-menu
  private menu: BehaviorSubject<Menu[]> = new BehaviorSubject<Menu[]>(null);
  //  bs for option
  private option: BehaviorSubject<Option> = new BehaviorSubject<Option>(null);
  //  bs for options
  private options: BehaviorSubject<Option[]> = new BehaviorSubject<Option[]>(null);
  //  bs for option types
  private optionTypes: BehaviorSubject<OptionType[]> = new BehaviorSubject<OptionType[]>(null);
  //  bs for option value
  private optionValue: BehaviorSubject<OptionValue> = new BehaviorSubject<OptionValue>(null);
  //  bs for option values
  private optionValues: BehaviorSubject<OptionValue[]> = new BehaviorSubject<OptionValue[]>(null);
  //  bs for options set
  private optionsSet: BehaviorSubject<OptionsSet> = new BehaviorSubject<OptionsSet>(null);
  //  bs for options sets
  private optionsSets: BehaviorSubject<OptionsSet[]> = new BehaviorSubject<OptionsSet[]>(null);
  //  bs for order
  private order: BehaviorSubject<Order> = new BehaviorSubject<Order>(null);
  //  bs for orders
  private orders: BehaviorSubject<Order[]> = new BehaviorSubject<Order[]>(null);
  //  bs for order priorities
  private orderPriorities: BehaviorSubject<OrderPriority[]> = new BehaviorSubject<OrderPriority[]>(null);
  //  bs for order statuses
  private orderStatuses: BehaviorSubject<OrderStatus[]> = new BehaviorSubject<OrderStatus[]>(null);
  //  bs for payment
  private payment: BehaviorSubject<Payment> = new BehaviorSubject<Payment>(null);
  //  bs for payments
  private payments: BehaviorSubject<Payment[]> = new BehaviorSubject<Payment[]>(null);
  //  bs for payment state
  private paymentState: BehaviorSubject<string> = new BehaviorSubject<string>(null);
  //  bs for payment state
  private paymentURL: BehaviorSubject<string> = new BehaviorSubject<string>(null);
  //  bs for product
  private product: BehaviorSubject<Product> = new BehaviorSubject<Product>(null);
  //  bs for products
  private products: BehaviorSubject<Product[]> = new BehaviorSubject<Product[]>(null);
  //  bs for product shadow
  private productShadow: BehaviorSubject<ProductShadow> = new BehaviorSubject<ProductShadow>(null);
  //  bs for product shadows
  private productShadows: BehaviorSubject<ProductShadow[]> = new BehaviorSubject<ProductShadow[]>(null);
  //  bs for purchase
  // private purchase: BehaviorSubject<Purchase> = new BehaviorSubject<Purchase>(null);
  // //  bs for purchases
  // private purchases: BehaviorSubject<Purchase[]> = new BehaviorSubject<Purchase[]>(null);
  //  bs for region
  private region: BehaviorSubject<Region> = new BehaviorSubject<Region>(null);
  //  bs for regions
  private regions: BehaviorSubject<Region[]> = new BehaviorSubject<Region[]>(null);
  //  bs for show category picture
  private showCategoryPicture: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);

  constructor(private http: HttpClient, private convertService: ConvertService) {
    //  link casts & bs for
    //  link casts & bs for address
    this.castAddress = this.address.asObservable();
    //  link casts & bs for addresses
    this.castAddresses = this.addresses.asObservable();
    // //  link casts & bs for assembly
    // this.castAssembly = this.assembly.asObservable();
    // //  link casts & bs for assemblies
    // this.castAssemblies = this.assemblies.asObservable();
    //  link casts & bs for brand
    this.castBrand = this.brand.asObservable();
    //  link casts & bs for brands
    this.castBrands = this.brands.asObservable();
    //  link casts & bs for cart
    this.castCart = this.cart.asObservable();
    //  link casts & bs for carts
    this.castCarts = this.carts.asObservable();
    //  link casts & bs for cart isShownBit
    this.castCartIsShownBit = this.cartIsShownBit.asObservable();
    //  link casts & bs for cart
    this.castCartItem = this.cartItem.asObservable();
    //  link casts & bs for carts
    this.castCartItems = this.cartItems.asObservable();
    //  link casts & bs for category
    this.castCategory = this.category.asObservable();
    //  link casts & bs for categories
    this.castCategoriesAll = this.categoriesAll.asObservable();
    //  link casts & bs for categories
    this.castCategoriesTop = this.categoriesTop.asObservable();
    //  link casts & bs for countries
    this.castCountry = this.country.asObservable();
    //  link casts & bs for countries
    this.castCountries = this.countries.asObservable();
    //  link casts & bs for countriesShort
    this.castCountriesShort = this.countriesShort.asObservable();
    //  link casts & bs for countriesShort
    this.castCountriesList = this.countriesList.asObservable();
    //  link casts & bs for customer
    this.castCustomer = this.customer.asObservable();
    //  link casts & bs for customers
    this.castCustomers = this.customers.asObservable();
    //  link casts & bs for customer rank
    this.castCustomerRank = this.customerRank.asObservable();
    //  link casts & bs for customers rank
    this.castCustomerRanks = this.customerRanks.asObservable();
    //  link casts & bs for currency
    this.castCurrency = this.currency.asObservable();
    //  link casts & bs for currencies
    this.castCurrencies = this.currencies.asObservable();
    //  link casts & bs for delivery
    this.castDelivery = this.delivery.asObservable();
    //  link casts & bs for deliveries
    this.castDeliveries = this.deliveries.asObservable();
    //  link casts & bs for delivery cost
    this.castDeliveryCost = this.deliveryCost.asObservable();
    // //  link casts & bs for incoming
    // this.castIncoming = this.incoming.asObservable();
    // //  link casts & bs for incomings
    // this.castIncomings = this.incomings.asObservable();
    //  link casts & bs for image
    this.castImage = this.image.asObservable();
    //  link casts & bs for images
    this.castImages = this.images.asObservable();
    //  link casts & bs for item
    this.castItem = this.item.asObservable();
    //  link casts & bs for items
    this.castItems = this.items.asObservable();
    //  link casts & bs for item shadow
    this.castItemShadow = this.itemShadow.asObservable();
    //  link casts & bs for items shadow
    this.castItemShadows = this.itemShadows.asObservable();
    //  link casts & bs for localizations
    this.castLocalizations = this.localizations.asObservable();
    //  link casts & bs for language
    this.castLanguage = this.language.asObservable();
    //  link casts & bs for languages
    this.castLanguages = this.languages.asObservable();
    //  link casts & bs for languages
    this.castLanguages = this.languages.asObservable();
    //  link casts & bs for manufacturer
    this.castManufacturer = this.manufacturer.asObservable();
    //  link casts & bs for manufacturers
    this.castManufacturers = this.manufacturers.asObservable();
    //  link casts & bs for admin-menu
    this.castMenu = this.menu.asObservable();
    //  link casts & bs for option
    this.castOption = this.option.asObservable();
    //  link casts & bs for options
    this.castOptions = this.options.asObservable();
    //  link casts & bs for option type
    this.castOptionTypes = this.optionTypes.asObservable();
    //  link casts & bs for option value
    this.castOptionValue = this.optionValue.asObservable();
    //  link casts & bs for option values
    this.castOptionValues = this.optionValues.asObservable();
    //  link casts & bs for options set
    this.castOptionsSet = this.optionsSet.asObservable();
    //  link casts & bs for options sets
    this.castOptionsSets = this.optionsSets.asObservable();
    //  link casts & bs for order
    this.castOrder = this.order.asObservable();
    //  link casts & bs for orders
    this.castOrders = this.orders.asObservable();
    //  link casts & bs for order priorities
    this.castOrderPriorities = this.orderPriorities.asObservable();
    //  link casts & bs for order statuses
    this.castOrderStatuses = this.orderStatuses.asObservable();
    //  link casts & bs for payment
    this.castPayment = this.payment.asObservable();
    //  link casts & bs for payments
    this.castPayments = this.payments.asObservable();
    //  link casts & bs for payment state
    this.castPaymentState = this.paymentState.asObservable();
    //  link casts & bs for payment state
    this.castPaymentURL = this.paymentURL.asObservable();
    //  link casts & bs for product
    this.castProduct = this.product.asObservable();
    //  link casts & bs for products
    this.castProducts = this.products.asObservable();
    //  link casts & bs for product shadow
    this.castProductShadow = this.productShadow.asObservable();
    //  link casts & bs for product shadows
    this.castProductShadows = this.productShadows.asObservable();
    // //  link casts & bs for purchase
    // this.castPurchase = this.purchase.asObservable();
    // //  link casts & bs for purchases
    // this.castPurchases = this.purchases.asObservable();
    //  link casts & bs for region
    this.castRegion = this.region.asObservable();
    //  link casts & bs for regions
    this.castRegions = this.regions.asObservable();
    //  привязываем каст и БС для бита отображения картинки категории
    this.castShowCategoryPicture = this.showCategoryPicture.asObservable();

    //  первичная инициализация
    this.initial();
  }

  //  ****  CART  ********************
  //  добавить товар в корзину
  //  product - товар
  //  optionValues - опции
  //  amount - количество
  public addProductToCart(product: Product, optionValues: OptionValue[], amount: number) {
    //  создаем временную корзину
    const currentCart: Cart = this.cart.value;
    //  вводим бит добавления продукта, чтоб не добавить дважды
    let productHasAdd: boolean = false;
    console.log('addProductToCart START:', currentCart.cartItems);
    //  есть ли хоть один CartItem
    if (currentCart && currentCart.cartItems && currentCart.cartItems.length) {
      console.log('корзина не пустая', currentCart);
      //  пробуем найти товар
      const cartItemsWithProduct = currentCart.cartItems.filter((cartItem: CartItem) => cartItem.productID === product.id);
      if (cartItemsWithProduct.length) {
        //  перебираем все продукты
        cartItemsWithProduct.forEach(cartItemWithProduct => {
          //  если опций нет сразу добавляем нужное количество
          if (optionValues.length === 0 && !productHasAdd) {
            console.log('товары идентичны (нет опций)');
            cartItemWithProduct.orderCount += amount;
            //  и проставляем бит
            productHasAdd = true;
          }
          //  если опция одна
          if (optionValues.length === 1 && !productHasAdd) {
            //  проверяем на совпадение
            if (optionValues[0].id === cartItemWithProduct.optionValues[0].id) {
              console.log('товары идентичны (одна опция)');
              cartItemWithProduct.orderCount += amount;
              //  и проставляем бит
              productHasAdd = true;
            }
          }
          //  если больше одной опции
          if (optionValues.length > 1 && !productHasAdd) {
            if (this.compareOptionValues(optionValues, cartItemWithProduct.optionValues)) {
              //  товары идентичны
              console.log('товары идентичны (опций больше одной)');
              //  просто добавляем еще один в наличие
              // const cartIndex = currentCart.cartItems.findIndex(cartItem => cartItem.id === cartItemWithProduct.id);
              cartItemWithProduct.orderCount += amount;
            }
          }
        });

        //  если совпадений не было
        if (!productHasAdd) {
          console.log('совпадения по продукту нет' );
          const newCartItem: CartItem = {
            product,
            productID: product.id,
            optionValues,
            orderCount: amount,
            availableCount: null,
            cartID: currentCart.id
          };

          currentCart.cartItems.push(newCartItem);
        }
      } else {
        //  совпадения по продукту нет

        console.log('совпадения по продукту нет' );
        const newCartItem: CartItem = {
          product,
          productID: product.id,
          optionValues,
          orderCount: amount,
          availableCount: null,
          cartID: currentCart.id
        };

        currentCart.cartItems.push(newCartItem);
      }
    } else {
      //  корзина пустая

      console.log('корзина пустая' );
      const newCartItem: CartItem = {
        product,
        productID: product.id,
        optionValues,
        orderCount: amount,
        availableCount: null,
        cartID: currentCart.id
      };

      currentCart.cartItems.push(newCartItem);
    }
    //  публикуем корзину
    console.log('addProductToCart END: ', currentCart);
    //  публикуем корзину
    this.publishCart(currentCart);
  }

  //  показать / скрыть корзину
  public changeCartVisibility(cartIsShownBit: boolean) {
    //  публикуем новый полученный бит видимости меню
    this.cartIsShownBit.next(cartIsShownBit);
  }

  //  создать корзину
  public createCart(cart: Cart) {
    const serverCart: CartServer = this.convertService.cartClientToServer(cart);
    this.http.put<CartServer>(baseProto + baseUrl + urlCart.create, serverCart)
      .pipe(
        map(cartServer => this.convertService.cartServerToClient(cartServer)),
        tap((cartToPublish: Cart) => this.publishCart(cartToPublish)),
        catchError(err => {
          console.log(err, 'ERROR in createCart service');
          return of(null);
        })
      ).subscribe();
  }

  //  опубликовать корзину
  public getCart() {
    console.log('getCart');
    if (!localStorage.getItem('cart')) {
      console.log('New cart');
      const cart: Cart = {
        cartItems: [],
        order: null,
        orderID: null,
        delivery: null,
        deliveryID: null,
        payment: null,
        paymentID: null,
        address: null,
        addressID: null,
        customer: null,
        customerID: null,
        comment: null
      };
      this.createCart(cart);
    } else {
      this.cart.next(JSON.parse(localStorage.getItem('cart')));
      console.log('Cart is exist: ', JSON.parse(localStorage.getItem('cart')));
    }
  }

  public publishCart(cart: Cart) {
    //  проверка, что передали непустоту
    if (cart != null) {
      localStorage.setItem('cart', JSON.stringify(cart));
      console.log('Set cart: ', JSON.parse(localStorage.getItem('cart')));
      //  публикуем новое значение
      this.cart.next(cart);
    } else {
      //  публикуем null
      this.cart.next(null);
    }
  }

  private publishCartItems(cartItem: CartItem) {
    //  проверка, что передали непустоту
    if (cartItem != null) {
      //  публикуем новое значение
      this.cartItem.next(cartItem);
    } else {
      //  публикуем null
      this.cartItem.next(null);
    }
  }

  //  ****  CATEGORY  ********************
  //  получение значений стран
  public getCategories() {
    //  дергаем соответствующий сервис
    this.http.get<CategoryServer[]>(baseProto + baseUrl + urlCategory.readAll)
      .pipe(
        //  конвертация массива
        map(categoriesServer =>
          categoriesServer.map(categoryServer => this.convertService.categoryServerToClient(categoryServer))),
        //  публикуем список
        tap(categories => {
          //  проверка, что передали непустоту
          if (categories != null) {
            //  публикуем новое значение
            this.categoriesTop.next(categories);
          } else {
            //  публикуем null
            this.categoriesTop.next(null);
          }
        }),
        catchError(err => {
          console.log(err, 'ERROR in getCategories service');
          return of(null);
        })
      ).subscribe();
  }

  //  получение значений конкретной страны
  public getCategoryByID(categoryID: string) {
    //  дергаем соответствующий сервис
    this.http.get<CategoryServer>(baseProto + baseUrl + urlCategory.readCurrent, {
      params: new HttpParams().set(`id`, categoryID)
    })
      .pipe(
        //  конвертация массива
        map(categoryServer => this.convertService.categoryServerToClient(categoryServer)),
        //  публикуем список
        tap(category => {
          //  проверка, что передали непустоту
          if (category != null) {
            //  публикуем новое значение
            console.log('dataService - getCategoryByID: ', category);
            this.category.next(category);
          } else {
            //  публикуем null
            this.category.next(null);
          }
        }),
        catchError(err => {
          console.log(err, 'ERROR in getCategoryByID service');
          return of(null);
        })
      ).subscribe();
  }

  //  ****  BROCKER *********************
  public getContent(seoLink: string): Observable<any> {
    console.log('dataService - getContent - seoLink: ', seoLink);
    //  дергаем соответствующий сервис
    return this.http.get<any>(baseProto + baseUrl + urlBrocker.readContent, {
      params: new HttpParams().set(`seolink`, seoLink)
    })
      .pipe(
        //  конверт данных
        map(data => {
          console.log('dataService - getContent - data: ', data);
          if (data && data.type === 'category') {
            // const categoryServer: CategoryServer = data;
            const category: Category = this.convertService.categoryServerToClient(data as CategoryServer);
            console.log('dataService - getContent - category ', category);
            this.getCategoryByID(String(category.id));
            this.getProductsByCategory(category);
            return category;
          } else if (data && data.type === 'product') {
            // const productServer: ProductServer = data;
            const product: Product = this.convertService.productServerToClient(data as ProductServer);
            console.log('dataService - getContent - product:', product);
            return product;
          } else {
            console.log('dataService - getContent - unknown:', data);
            return data;
          }
        }),
        //  если ошибка
        catchError(err => {
          console.log(err, 'ERROR in getContent service');
          return of(null);
        })
      );
  }

  //  ****  COUNTRY  ********************
  //  получение значений стран
  public getCountriesShort() {
    //  дергаем соответствующий сервис
    this.http.get<CountryShortServer[]>(baseProto + baseUrl + urlCountry.readAllShort)
      .pipe(
        //  конвертация массива
        map(countriesServer =>
          countriesServer.map(countryShortServer => this.convertService.countryShortServerToClient(countryShortServer))),
        //  публикуем список
        tap(countriesShort => {
          //  проверка, что передали непустоту
          if (countriesShort != null) {
            console.log('countriesShort: ', countriesShort);
            const countriesList: CountryShort[] = [];
            countriesShort.forEach(countryShort => {
              countriesList.push(countryShort);
            });
            //  публикуем новое значение
            this.countriesShort.next(countriesShort);
            this.countriesList.next(countriesList);
          } else {
            //  публикуем null
            this.countriesShort.next(null);
          }
        }),
        catchError(err => {
          console.log(err, 'ERROR in getCountriesShort service');
          return of(null);
        })
      ).subscribe();
  }

  //  получение значений конкретной страны
  public getCountryByID(countryID: string) {
    //  дергаем соответствующий сервис
    this.http.get<CountryServer>(baseProto + baseUrl + urlCountry.readCurrent, {
      params: new HttpParams().set(`id`, countryID)
    })
      .pipe(
        //  конвертация массива
        map(countryServer => this.convertService.countryServerToClient(countryServer)),
        //  публикуем список
        tap(country => {
          //  проверка, что передали непустоту
          if (country != null) {
            //  публикуем новое значение
            this.country.next(country);
          } else {
            //  публикуем null
            this.country.next(null);
          }
        }),
        catchError(err => {
          console.log(err, 'ERROR in getCountryByID service');
          return of(null);
        })
      ).subscribe();
  }

  //  ****  CURRENCY  ********************
  //  получение значений валюты
  public getCurrencies() {
    //  дергаем соответствующий сервис
    this.http.get<CurrencyServer[]>(baseProto + baseUrl + urlCurrency.readAll)
      .pipe(
        //  конвертация массива
        map(currenciesServer =>
          currenciesServer.map(currencyServer => this.convertService.currencyServerToClient(currencyServer))),
        //  публикуем список
        tap(currencies => {
          //  проверка, что передали непустоту
          if (currencies != null) {
            //  публикуем новое значение
            this.currencies.next(currencies);
            if (currencies && currencies.length) {
              this.setCurrency(currencies[0]);
              console.log('CURRENCY is: ', currencies[0]);
            }
          } else {
            //  публикуем null
            this.currencies.next(null);
          }
        }),
        catchError(err => {
          console.log(err, 'ERROR in getCurrencies service');
          return of(null);
        })
      ).subscribe();
  }

  //  задать текущую валюту
  public setCurrency(currency: Currency) {
    //  задаем новое значение валюты
    if (currency) {
      this.currency.next(currency);
    }
  }

  //  ****  DELIVERIES  ********************


  //  получение delivery
  //  deliveryID - идентификатор торговой марки
  public getDeliveryByID(deliveryID: string) {
    //  проверка, что передали непустоту
    if (deliveryID) {
      //  дергаем соответствующий сервис
      this.http.get<DeliveryServer>(baseProto + baseUrl + urlDelivery.readCurrent, {
        params: new HttpParams().set(`id`, deliveryID)
      })
        .pipe(
          map(deliveryServer => this.convertService.deliveryServerToClient(deliveryServer)),
          //  публикация
          tap(delivery => {
            //  проверка, что передали непустоту
            if (deliveryID) {
              //  публикация  delivery
              this.delivery.next(delivery);
            }
          }),
          catchError(err => {
            console.log(err, 'ERROR in getDeliveryByID service');
            return of(null);
          })
        ).subscribe();
    }
  }
  //  получение цены delivery
  public getDeliveryCost(cart: Cart) {
    console.log('cart: ', cart);
    //  проверка, что передали непустоту
    if (cart && cart.deliveryID && cart.cartItems && cart.cartItems.length) {
      //  дергаем соответствующий сервис
      this.http.post<number>(baseProto + baseUrl + urlDelivery.getCost, cart)
        .pipe(
          //  публикация
          tap(deliveryCost => {
            //  проверка, что передали непустоту
            if (deliveryCost) {
              //  публикация  delivery
              this.deliveryCost.next(deliveryCost);
            }
          }),
          catchError(err => {
            console.log(err, 'ERROR in getDeliveryCost service');
            return of(null);
          })
        ).subscribe();
    }
  }

  //  получение значений валюты
  public getDeliveries() {
    //  дергаем соответствующий сервис
    this.http.get<DeliveryServer[]>(baseProto + baseUrl + urlDelivery.readAll)
      .pipe(
        //  конвертация массива
        map(deliveriesServer =>
          deliveriesServer.map(deliveryServer => this.convertService.deliveryServerToClient(deliveryServer))),
        //  публикуем список
        tap(deliveries => {
          //  проверка, что передали непустоту
          if (deliveries != null) {
            //  публикуем новое значение
            this.deliveries.next(deliveries);
          } else {
            //  публикуем null
            this.deliveries.next(null);
          }
        }),
        catchError(err => {
          console.log(err, 'ERROR in getDeliveries service');
          return of(null);
        })
      ).subscribe();
  }

  //  LANGUAGE
  //  получение значений языков
  public getLanguages() {
    //  дергаем соответствующий сервис
    this.http.get<LanguageServer[]>(baseProto + baseUrl + urlLanguage.readAll)
      .subscribe((languagesServer: LanguageServer[]) => {
        // const serverCurrencies: CurrencyServer = JSON.parse(resp);
        const languages: Language[] = [];
        languagesServer.forEach((languageServer: LanguageServer) => {
          languages.push(this.convertService.languageServerToClient(languageServer));
        });
        console.log('getLanguages', languages);
        //  публикуем новое значение
        this.languages.next(languages);
        if (languages && languages.length) {
          if (!localStorage.getItem('language')) {
            this.setLanguage(languages[0]);
          } else {
            this.setLanguage(JSON.parse(localStorage.getItem('language')));
          }
          console.log('LANGUAGE is: ', languages[0]);
        }
      }, error => {
        console.log(error, 'ERROR in getLanguages service');
      });
  }

  //  задать текущий язык
  public setLanguage(language: Language) {
    //  задаем новое значение языка
    if (language) {
      localStorage.setItem('language', JSON.stringify(language));
      this.language.next(language);
      //  передергиваем страну, так как помеялась локаль
      this.getCountriesShort();
    }
  }

  //  LOCALIZATION
  //  получение значений локализации
  public getLocalizations() {
    //  дергаем соответствующий сервис
    this.http.get<LocalizationServer[]>(baseProto + baseUrl + urlLocalization.readAll)
      .subscribe((localizationServers: LocalizationServer[]) => {
        const localizations: Localization[] = [];
        localizationServers.forEach((localizationServer: LocalizationServer) => {
          localizations.push(this.convertService.localizationServerToClient(localizationServer));
        });
        console.log('getLocalizations', localizations);
        //  публикуем новое значение
        this.localizations.next(localizations);
      }, error => {
        console.log(error, 'ERROR in getLocalizations service');
      });
  }

  //  ORDERS
  //  создаем пустой заказ
  public createOrder(newOrder: Order): Observable<Order> {
    //  конвертация
    const serverOrder: OrderServer = this.convertService.orderClientToServer(newOrder);
    console.log('sended order: ', serverOrder);
    return this.http.put<OrderServer>(baseProto + baseUrl + urlOrder.create, serverOrder)
      .pipe(
        map(orderServer => this.convertService.orderServerToClient(orderServer)),
        tap((order: Order) => {
          if (order) {
            //  публикуем ордер
            this.order.next(order);
            console.log('dataService - paymentURL: ', order);
            this.paymentURL.next(order.paymentURL);
          }
        }),
        catchError(err => {
          console.log(err, 'ERROR in createOrder service');
          return of(null);
        })
      );
  }

  //  PRODUCTS
  //  получение значений брэнда
  //  category - категория продукта
  public getProductsByCategory(category: Category) {
    //  если что-то передали
    if (category && category.id) {
      //  дергаем соответствующий сервис
      this.http.post<ProductServer[]>(baseProto + baseUrl + urlProduct.readAllByCategory, category)
        .pipe(
          //  конвертация массива
          map(productsServer =>
            productsServer.map(productServer => this.convertService.productServerToClient(productServer))),
          //  публикуем список
          tap(products => {
            //  проверка, что передали непустоту
            if (products != null) {
              //  публикуем новое значение
              this.category.next(category);
              this.products.next(products);
            } else {
              //  публикуем null
              this.products.next(null);
            }
          }),
          catchError(err => {
            console.log(err, 'ERROR in getProductsByCategory service');
            return of(null);
          })
        ).subscribe();
    }
  }
  //  получение значений products
  //  category - категория продукта
  public getProductsByCategoryID(categoryID: string) {
    //  если что-то передали
    if (categoryID) {
      //  дергаем соответствующий сервис
      this.http.get<CategoryServer>(baseProto + baseUrl + urlCategory.readCurrent, {
        params: new HttpParams().set(`id`, categoryID)
      })
        .pipe(
          //  конвертация массива
          map(categoryServer => this.convertService.categoryServerToClient(categoryServer)),
          //  публикуем список
          tap(category => {
            //  проверка, что передали непустоту
            if (category != null) {
              //  публикуем новое значение
              console.log('dataService - getCategoryByID: ', category);
              this.category.next(category);
              //  дергаем соответствующий сервис
              this.http.post<ProductServer[]>(baseProto + baseUrl + urlProduct.readAllByCategory, category)
                .pipe(
                  //  конвертация массива
                  map(productsServer =>
                    productsServer.map(productServer => this.convertService.productServerToClient(productServer))),
                  //  публикуем список
                  tap(products => {
                    //  проверка, что передали непустоту
                    if (products != null) {
                      //  публикуем новое значение
                      this.products.next(products);
                    } else {
                      //  публикуем null
                      this.products.next(null);
                    }
                  }),
                  catchError(err => {
                    console.log(err, 'ERROR in getProductsByCategoryID service');
                    return of(null);
                  })
                ).subscribe();
            } else {
              //  публикуем null
              this.category.next(null);
            }
          }),
          catchError(err => {
            console.log(err, 'ERROR in getCategoryByID service');
            return of(null);
          })
        ).subscribe();
    }
  }

  //  получение значений брэнда
  //  productID - идентификатор торговой марки
  public getProductByID(productID: string) {
    //  если что-то передали
    if (productID) {
      //  дергаем соответствующий сервис
      this.http.get<ProductServer>(baseProto + baseUrl + urlProduct.readCurrent, {
        params: new HttpParams().set(`id`, productID)
      })
        .subscribe((productServer: ProductServer) => {
          const newProduct: Product = this.convertService.productServerToClient(productServer);
          //  публикуем новое значение
          console.log('newProduct in getProductByID: ', newProduct);
          this.product.next(newProduct);
        }, error => {
          console.log(error, 'ERROR in getProductByID service');
        });
    }
  }

  //  получение значений продуктов
  public getProducts() {
    //  дергаем соответствующий сервис
    this.http.get<ProductServer[]>(baseProto + baseUrl + urlProduct.readAll)
      .subscribe((productsServer: ProductServer[]) => {
        //  дергаем функцию публикации
        this.publishProducts(productsServer);
      }, error => {
        console.log(error, 'ERROR in getProducts service');
      });
  }

  //  получение значений продуктов
  public getProductsShort() {
    //  дергаем соответствующий сервис
    this.http.get<ProductServer[]>(baseProto + baseUrl + urlProduct.readAllShort)
      .subscribe((productsServer: ProductServer[]) => {
        //  дергаем функцию публикации
        this.publishProducts(productsServer);
      }, error => {
        console.log(error, 'ERROR in getProductsShort service');
      });
  }

  //  публикация прилетевшего списка продуктов - вспомогательная функция
  //  productsServer - массив продуктов серверного типа
  private publishProducts(productsServer: ProductServer[]) {
    console.log('publishProducts', productsServer);
    const newProducts: Product[] = [];
    if (productsServer) {
      productsServer.forEach((productServer: ProductServer) => {
        newProducts.push(this.convertService.productServerToClient(productServer));
      });
      //  публикуем новое значение
      this.products.next(newProducts);
    } else {
      //  публикуем null
      this.products.next(null);
    }
  }

  //  PAYPAL
  //  получение значений брэнда
  //  productID - идентификатор торговой марки
  // public paypalCheckPayment(captureID: string, amount: number, currencyCode: string) {
  //   //  если что-то передали
  //   if (captureID && amount) {
  //     //  дергаем соответствующий сервис
  //     this.http.get<string>(baseProto + baseUrl + urlPaypal.check, {
  //       params: new HttpParams()
  //         .set(`capture_id`, captureID)
  //         .append(`amount`, String(amount))
  //         .append(`currency_code`, currencyCode)
  //     })
  //       .subscribe((response: string) => {
  //         console.log('Result of payment: ', response);
  //         this.paymentState.next(response);
  //       }, error => {
  //         console.log(error, 'ERROR in paypalCheckPayment service');
  //         this.paymentState.next(null);
  //       });
  //   }
  // }

  //  **************************
  //  REGIONS
  //  получение значений региона
  //  regionID - идентификатор региона
  public getRegionByID(regionID: string) {
    //  если что-то передали
    if (regionID) {
      //  дергаем соответствующий сервис
      this.http.get<RegionServer>(baseProto + baseUrl + urlRegion.readCurrent, {
        params: new HttpParams().set(`id`, regionID)
      })
        .subscribe((regionServer: RegionServer) => {
          const newRegion: Region = this.convertService.regionServerToClient(regionServer);
          //  публикуем новое значение
          console.log('newProduct in getProductByID: ', newRegion);
          this.region.next(newRegion);
        }, error => {
          console.log(error, 'ERROR in getRegionByID service');
        });
    }
  }

  //  список регионов по идентификатору страны
  public getRegionsByCountryID(countryID: string) {
    //  если передали не пустое
    if (countryID && countryID.length) {
      //  дергаем соответствующий сервис
      this.http.get<RegionServer[]>(baseProto + baseUrl + urlRegion.readAllByCountryID, {
        params: new HttpParams().set(`countryID`, countryID)
      })
        .pipe(
          //  конвертация массива
          map(regionsServer =>
            regionsServer.map(regionServer => this.convertService.regionServerToClient(regionServer))),
          //  публикуем список
          tap(regions => this.publishRegions(regions)),
          catchError(err => {
            console.log(err, 'ERROR in getRegions service');
            return of(null);
          })
        ).subscribe();
    }

  }

  //  публикация прилетевшего списка  стран - вспомогательная функция
  private publishRegions(regions: Region[]) {
    //  проверка, что передали непустоту
    if (regions != null) {
      //  публикуем новое значение
      this.regions.next(regions);
    } else {
      //  публикуем null
      this.regions.next(null);
    }
  }

  //  **********************************
  //  получение значений меню
  public getMenu() {
    //  создаем переменную под меню
    const newMenu: Menu[] = GenerateService.generateMenu();
    //  временно наполняем из темпорари
    // this.temporaryMenu.forEach(menuItem => {
    //   newMenu.push(menuItem);
    // });
    // //  дергаем соответствующий сервис
    // this.http.get<CurrencyServer[]>(baseProto + baseUrl + currenciesGet)
    //   .subscribe((currenciesServer: CurrencyServer[]) => {
    //     // const serverCurrencies: CurrencyServer = JSON.parse(resp);
    //     const newCurrencies: Currency[] = [];
    //     currenciesServer.forEach((currencyServer: CurrencyServer) => {
    //       newCurrencies.push(this.convertService.currencyServerToClient(currencyServer));
    //     });
    //     //  публикуем новое значение
    //     this.currencies.next(newCurrencies);
    //   }, error => {
    //     console.log(error, 'ERROR in getMenu service');
    //   });
    //  публикуем новое значение
    this.menu.next(newMenu);
  }

  //  ****  PAYMENTS  ********************


  //  получение payment
  //  paymentID - идентификатор способа оплаты
  public getPaymentByID(paymentID: string) {
    //  дергаем соответствующий сервис
    this.http.get<PaymentServer>(baseProto + baseUrl + urlPayment.readCurrent, {
      params: new HttpParams().set(`id`, paymentID)
    })
      .pipe(
        //  конвертация массива
        map(paymentsServer => this.convertService.paymentServerToClient(paymentsServer)),
        //  публикация  массива
        tap(payment => {
          //  проверка, что передали непустоту
          if (paymentID) {
            //  публикация  payment
            this.payment.next(payment);
          } else {
            //  публикация  null
            this.payment.next(null);
          }
        }),
        catchError(err => {
          console.log(err, 'ERROR in getPaymentByID service');
          return of(null);
        })
      ).subscribe();
  }

  //  получение вариантов оплаты
  public getPayments() {
    //  дергаем соответствующий сервис
    this.http.get<PaymentServer[]>(baseProto + baseUrl + urlPayment.readAll)
      .pipe(
        //  конвертация массива
        map(paymentsServer =>
          paymentsServer.map(paymentServer => this.convertService.paymentServerToClient(paymentServer))),
        //  публикуем список
        tap(payments => {
          //  проверка, что передали непустоту
          if (payments != null) {
            //  публикуем новое значение
            this.payments.next(payments);
          } else {
            //  публикуем null
            this.payments.next(null);
          }
        }),
        catchError(err => {
          console.log(err, 'ERROR in getPayments service');
          return of(null);
        })
      ).subscribe();
  }

  //  CATEGORY PICTURES
  public putShowCategoryPictures(isShown: boolean) {
    //  публикуем бит
    this.showCategoryPicture.next(isShown);
  }

  //  SBERBANK
  //  получение значений конкретной страны
  public paypalCheckStatus(paypalToken: string) {
    //  дергаем соответствующий сервис
    this.http.get<OrderServer>(baseProto + baseUrl + urlPaypal.checkStatus, {
      params: new HttpParams().set(`token`, paypalToken)
    })
      .pipe(
        //  конвертация массива
        map(orderServer => this.convertService.orderServerToClient(orderServer)),
        //  публикуем список
        tap(order => {
          //  проверка, что передали непустоту
          if (order != null) {
            //  публикуем новое значение
            console.log('dataService - paypalCheckStatus: ', order);
            this.order.next(order);
          } else {
            //  публикуем null
            this.order.next(null);
          }
        }),
        catchError(err => {
          console.log(err, 'ERROR in paypalCheckStatus service');
          return of(null);
        })
      ).subscribe();
  }

  //  SBERBANK
  //  получение значений конкретной страны
  public sberbankCheckStatus(sberbankOrderID: string) {
    //  дергаем соответствующий сервис
    this.http.get<OrderServer>(baseProto + baseUrl + urlSberbank.checkStatus, {
      params: new HttpParams().set(`orderID`, sberbankOrderID)
    })
      .pipe(
        //  конвертация массива
        map(orderServer => this.convertService.orderServerToClient(orderServer)),
        //  публикуем список
        tap(order => {
          //  проверка, что передали непустоту
          if (order != null) {
            //  публикуем новое значение
            console.log('dataService - sberbankCheckStatus: ', order);
            this.order.next(order);
          } else {
            //  публикуем убитую оплату
            order = GenerateService.createOrder();
            order.statusID = 1;
            this.order.next(order);
          }
        }),
        catchError(err => {
          console.log(err, 'ERROR in sberbankCheckStatus service');
          const order: Order = GenerateService.createOrder();
          order.statusID = 1;
          this.order.next(order);
          return of(null);
        })
      ).subscribe();
  }

  //  функция инициализации
  private initial() {
    console.log('initial');
    this.getLanguages();
    this.getLocalizations();
    this.getCategories();
    this.getCountriesShort();
    this.getCurrencies();
    // this.getProductsShort();
    this.getProducts();
    this.getDeliveries();
    this.getPayments();
    this.getCart();
    // this.getOrderPriorities();
    // this.getOrderStatuses();
  }

  //  функция сравнения значений опций - нужна для сравнения товаров в корзине
  //  ov1 - опции добавляемого продукта
  //  ov2 -
  private compareOptionValues(ov1: OptionValue[], ov2: OptionValue[]): boolean {
  //   в начале тупо считаем опции
    if (ov1 == null || ov1.length !== ov2.length) {
      // длины разные - товары разные
      return false;
    } else {
      //  если опция одна - просто сравниваем результаты
      if (ov1.length === 1) {
        // возвращаем результат совпадения
        return ov1[0].id === ov2[0].id;
      } else {
        //  опций больше одной
        //  задаем начальные переменные
        let count: number = ov2.length;
        //  перебираем элементы первого массива
        ov1.forEach((OV1: OptionValue) => {
          //  уменьшаем на один счетчик во втором массиве
          count--;
          //  ищем во втором массиве элемент из первого
          const index = ov2.findIndex((OV2: OptionValue) => OV2.id === OV1.id);
          //  если есть удаляем
          if (index !== -1) {
            ov2.splice(index, 1);
          }
        });
        //  возвращаем результат сравнения
        return count === 0;
      }
    }
  }
}
