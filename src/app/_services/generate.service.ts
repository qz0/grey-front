import { Injectable } from '@angular/core';
import {Menu} from '../_models/menu.types';
import {Cart, CartItem} from '../_models/cart.types';
import {Product} from '../_models/product.types';
import {Address} from '../_models/address.types';
import {Order} from '../_models/order.types';
import {Item} from '../_models/item.types';
import {LocalizedDescription, LocalizedText} from '../_models/localization.types';
import {Image} from '../_models/image.types';
import {Currency} from '../_models/currency.types';

@Injectable({
  providedIn: 'root'
})
export class GenerateService {

  constructor() { }

  //  создать пустую картинку
  public static createImage(type?: number): Image {
    // const randomString: string = String(Math.floor(Math.random() * 1000) + 1);
    return {
      priority: 5,
      name: 'default',
      url: 'none',
      options: null,
      optionsID: null,
      type: {
        id: 1,
        name: 'default',
        path: 'default'
      },
      typeID: type ? type : 1,
    };
  }

  //  создать пустую локаль
  public static createLocale(): LocalizedDescription[] {
    const randomString: string = String(Math.floor(Math.random() * 1000) + 1);
    return [{
      language: 'RU',
      title: 'Заголовок ' + randomString,
      name: 'Название ' + randomString,
      description: 'Название ' + randomString,
    },
      {
        language: 'EN',
        title: 'Title ' + randomString,
        name: 'Name ' + randomString,
        description: 'Description ' + randomString,
      }
    ];
  }

  //  создать пустую локаль
  public static createLocaleText(): LocalizedText[] {
    const randomString: string = String(Math.floor(Math.random() * 1000) + 1);
    return [{
      language: 'RU',
      text: 'Текст ' + randomString,
    },
      {
        language: 'EN',
        text: 'Text ' + randomString,
      }
    ];
  }


  //  создать случайную сборку
  public static createOrder(): Order {
    // String(Math.floor(Math.random() * 1000000) + 1),
    // console.log('generateOrder');
    //  variable
    let order: Order;
    const cartItems: CartItem[] = [];
    const items: Item[] = [];

    // console.log('generateOrder - order');
    //  contain
    order = {
      orderID: null,  //  Идентификатор для людей
      cartItems, // список продуктов из корзины
      items,  //  список айтемов
      status: null, // статус заказа
      statusID: 9, // ID - статус заказа 9 - missing - ожидает оплаты
      customer: null, //  покупатель
      createdShippingAddress: null, //  изначальный адрес доставки (взять оттуда поле комментария пользователя)
      currentShippingAddress: null,  //  измененный адрес доставки
      comments: '', //  комментарий
      delivery: null, //  доставка
      deliveryID: 0, //  доставка
      deliveryPackageNumber: 1, //  номер пакетов в заказе
      deliveryPackageCount: 1, //  количество пакетов в заказе
      deliveryPrice: 0, // доставка
      warehouseBoxNumber: 1, // номер коробки на складе
      weight: 0, // вес
      payment: null, //  оплата
      paymentID: 0, //  оплата
      paymentURL: '', //  url
      shop: null, //  shop
      deliveryCost: 0, //  итоговая цена
      productsCost: 0, //  итоговая цена
      totalCost: 0, //  итоговая цена
      currency: null, //  currency
      currencyID: 0,  // currencyID
      //  history
      trackingNumber: null, //  номер отслеживания
      priority: null, //  приоритет заказа
      priorityID: 2, //  ID - приоритет заказа 2 - standart
      sale: null, //  скидка
      //  shadow
      currencyShadow: null,
      shopShadow: null,
      itemsShadow: null, //  список покупок при создании
      shippingAddressShadow:  null,  //  измененный адрес доставки
    };
    //  return result
    return order;
  }

  //  создать случайную Address
  public static generateAddress(): Address {
    //  variable
    let address: Address;
    //  contain
    address = {
      zip: null, //  название
      country: null, //  страна
      region: null, //  регион
      city: null, //  город
      description: null, //  описание
      firstName: null,  //  имя получателя
      lastName: null,  //  имя получателя
      phone: null,  //  имя получателя
      email: null,  //  имя получателя
      comments: null,  //  имя получателя
    };
    //  return result
    return address;
  }

  //  create random cart item
  public static generateCart(): Cart {
    //  variable
    let cart: Cart;
    //  contain
    const cartItems: CartItem[] = [];
    for (let i = 0; i < 10; i++) {
      cartItems.push(GenerateService.generateCartItem());
    }
    cart = {
      cartItems,
      order: null,
      orderID: null,
      delivery: null,
      deliveryID: null,
      payment: null,
      paymentID: null,
      address: null,
      addressID: null,
      customer: null,
      customerID: null,
      comment: null
    };
    //  return result
    return cart;
  }

  //  create random cart item
  public static generateCartItem(product?: Product): CartItem {
    //  variable
    let cartItem: CartItem;
    //  contain
    cartItem = {
      product: product ? product : null, //   товар
      productID: null,  //  ID - товара
      optionValues: [],
      orderCount: product ? 1 : null, //  необходимо единиц товара
      availableCount: null, //  доступно единиц товара
      cartID: null
    };
    //  return result
    return cartItem;
  }

  //  currency
  public static generateCurrency(): Currency {
    // const randomDigit: string = String(Math.floor(Math.random() * 4) + 1);
    return {
      code: 'RU',  //  код
      title: 'Рубль',  //  выводимое название валюты
      multiplier: 1,  //  мнодитель
      symbol: '₽',  //  символ валюты
      before: false,  // положение символа валюты
    };
  }

  //  images
  public static generateImage(): Image {
    const randomDigit: string = String(Math.floor(Math.random() * 4) + 1);
    return {
      name: GenerateService.generateRandomString('Image'),  //  описание
      url: '/assets/img/user-' + randomDigit + '.png', //  url
      priority: 0, //  priority
      options: null,  //  options
      optionsID: null,
      type: null, //  type
      typeID: null,
    };
  }

  //  create random item
  public static generateItem(): Item {
    //  variable
    let item: Item;
    //  contain
    item = {
      product: this.generateProduct(), //  продукт
      productID: 0, //  продукт ID
      deliveryPackageNumber: 1,  //  в каком из пакетов заказа этот айтем
      itemOptions: [],  //  options
      optionValues: [],  //  optionValues
      optionsSets: [], //  optionsSet
      //  TODO: продумать статусы для разных складов
      status: null, //  status item
      placement: null,
      warehouseBoxNumber: Math.floor(Math.random() * 50) + 1, //  номер коробки на складе
      order: null, //  заказ
      comments: null, //  комментс
      price: null,  //  цена продажи
      currency: null, //  валюта на момент покупки
      sale: null, //  скидка
      assembled: false,
      checked: false,
    };
    //  return result
    return item;
  }

  //  admin-menu
  public static generateMenu(): Menu[] {
    return [
      {
        id: 1,
        name: 'brands',
        title: 'Brands',
        route: '/brands',
        children: []
      },
      {
        id: 2,
        name: 'images',
        title: 'Images',
        route: '/images',
        children: []
      },
      {
        id: 3,
        name: 'options',
        title: 'Options',
        route: '/options',
        children: []
      },
      {
        id: 4,
        name: 'categories',
        title: 'Menu',
        route: '/categories',
        children: []
      },
      {
        id: 10,
        name: 'cart',
        title: 'Cart',
        route: '/cart',
        children: []
      },
    ];
  }


  //  создать случайную сборку
  public static generateOrder(): Order {
    // String(Math.floor(Math.random() * 1000000) + 1),
    // console.log('generateOrder');
    //  variable
    let order: Order;
    const cartItems: CartItem[] = [];
    const items: Item[] = [];
    //  contain Array of Items
    for (let i = 0; i < 10; i++) {
      items.push(GenerateService.generateItem());
    }

    // console.log('generateOrder - order');
    //  contain
    order = {
      orderID: String(Math.floor(Math.random() * 1000000) + 1),  //  Идентификатор для людей
      cartItems,  //  список продуктов из корзины
      items,  //  список айтемов
      status: null, // статус заказа
      statusID: 1, // ID - статус заказа
      customer: null, //  покупатель
      createdShippingAddress: GenerateService.generateAddress(), //  изначальный адрес доставки (взять оттуда поле комментария пользователя)
      currentShippingAddress: GenerateService.generateAddress(),  //  измененный адрес доставки
      comments: '', //  комментарий
      delivery: null, //  доставка
      deliveryID: 0, //  доставка
      deliveryPackageNumber: 1, //  номер пакетов в заказе
      deliveryPackageCount: Math.floor(Math.random() * 10) + 1, //  количество пакетов в заказе
      deliveryPrice: GenerateService.generateRandomNumber(100), // доставка
      warehouseBoxNumber: 1, // номер коробки на складе
      weight: 0, // вес
      payment: null, //  оплата
      paymentID: 0, //  оплата
      paymentURL: '', //  url
      shop: null, //  shop
      deliveryCost: 0, //  итоговая цена
      productsCost: 0, //  итоговая цена
      totalCost: 0, //  итоговая цена
      currency: GenerateService.generateCurrency(), //  currency
      currencyID: 0,  // currencyID
      //  history
      trackingNumber: null, //  номер отслеживания
      priority: null, //  приоритет заказа
      priorityID: 2, //  ID - приоритет заказа
      sale: null, //  скидка
      //  shadow
      currencyShadow: null,
      shopShadow: null,
      itemsShadow: null, //  список покупок при создании
      shippingAddressShadow:  GenerateService.generateAddress(),  //  измененный адрес доставки
    };
    //  return result
    return order;
  }

  //  create random product
  public static generateProduct(): Product {
    //  variable
    let product: Product;
    //  contain
    product = {
      imageBack: GenerateService.generateImage(), // картинка
      imageBackID: 1,
      imageFront: GenerateService.generateImage(), // картинка
      imageFrontID: 1,
      productImages: [],
      items: [],  //  items
      localizedDescriptions: GenerateService.createLocale(), //  локализованное описание
      localizedFeatures: GenerateService.createLocaleText(), //  локализованые детали
      localizedMaterial: GenerateService.createLocaleText(), //  локализованые материалы
      localizedSEOs: GenerateService.createLocale(), //  сео расширенный
      seoLink: '',  //  сео линк
      brand: null, //  торговая марка
      brandID: null, //  ID - торговая марка
      manufacturer: null, //  производитель
      manufacturerID: null, //  ID - производитель
      price: null, //  цены
      categories: [], //  категории
      //  parameters
      restrictedDeliveries: null, //  запрещенные доставки
      weight: Math.floor(Math.random() * 100000) + 1, // вес
      volumeWeightX: Math.floor(Math.random() * 100000) + 1, //  объемный вес
      volumeWeightY: Math.floor(Math.random() * 100000) + 1, //  объемный вес
      volumeWeightZ: Math.floor(Math.random() * 100000) + 1, //  объемный вес
      fstek: '', // FSTEK
      warehousePosition: String(Math.floor(Math.random() * 100) + 1),  //  позиция на складе
      localizedProductTypes:  GenerateService.createLocale(), //  тип товара для таможни и не только
      article: 'string', //  артикул производителя
      priority: 1, // приоритет при отображении
      productAvailability: null, //  доступоность для вариантов продаж (пердзаказ, заказ по требованию и тд.)
      productAvailabilityID: 1, //  ID - доступоность для вариантов продаж (пердзаказ, заказ по требованию и тд.)
      //  tslint:disable-next-line
      productVisibility: null, //  отображать или нет в магазине (показывать, не показывать, показывать при наличии на складе)
      productVisibilityID: 1, //  отображать или нет в магазине (показывать, не показывать, показывать при наличии на складе)
      productOptions: null, //  список доуступных продукту опций
      optionsSets: null, //  список массивов опций
      sale: null, //  скидка
      type: null, //  скидка
    };
    //  return result
    return product;
  }

  //  генерация случайного числа
  public static generateRandomNumber(maxSize?: number): number {
    //  если текст передали
    if (maxSize && maxSize > 0) {
      //  если разделитель не передали
      return Math.floor(Math.random() * maxSize) + 1;
    } else {
      //  если текст не передали
      return Math.floor(Math.random() * 1000) + 1;
    }
  }


  //  генерация названия с случайными цифрами в конце
  public static generateRandomString(text?: string, splitter?: string): string {
    //  если текст передали
    if (text && text.length) {
      if (splitter && splitter.length) {
        //  если разделитель передали
        return text + splitter + String(Math.floor(Math.random() * 1000) + 1);
      } else {
        //  если разделитель не передали
        return text + ' ' + String(Math.floor(Math.random() * 1000) + 1);
      }
    } else {
      //  если текст не передали
      return 'text ' + String(Math.floor(Math.random() * 1000) + 1);
    }
  }
}
