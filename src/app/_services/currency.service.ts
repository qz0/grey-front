import { Injectable } from '@angular/core';
import {DataService} from './data.service';
import {Cart} from '../_models/cart.types';
import {Currency} from '../_models/currency.types';

@Injectable({
  providedIn: 'root'
})
export class CurrencyService {
  //  public
  public currency: Currency = null;   //  текущая корзина

  constructor(private dataService: DataService) {
    //  подписываемся на корзину
    this.dataService.castCurrency.subscribe((currency: Currency) => this.currency = currency);
  }

  //  конвертируем в другую валюту
  public convertToCurrency(cost: number): number {
    return Math.ceil(cost / this.currency.multiplier);
  }

  //  добавляем символ валюты в нужном месте
  public convertToMoney(cost: number): string {
    //  если все передали
    if (cost != null && this.currency) {
      //  если символ перед числом
      if (this.currency.before) {
        return this.currency.symbol + String(cost);
      } else {
        //  символ сзади
        return String(cost) + this.currency.symbol;
      }
    } else {
      //  возвращаем просто строку без символа валюты
      return String(cost);
    }
  }
}
