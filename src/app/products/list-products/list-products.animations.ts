// animations.ts
import { trigger, state, style, transition, animate } from '@angular/animations';

export const ListProductsAnimation = {
  // Вращение кнопки сворачивания
  arrowUpDown: trigger('arrowUpDown', [
    state('up', style({
      transform: 'rotate(0deg)',
      bottom: '-15px'
    })),
    state('down', style({
      transform: 'rotate(180deg)',
      bottom: '-45px'
    })),
    transition('up <=> down', [
      animate('.2s ease')
    ])
  ]),
  // Показать / скрыть картинку категории
  fadeInOut: trigger('fadeInOut', [
    state('up', style({
      height: '*',
      opacity: 1
    })),
    state('down', style({
      height: 0,
      opacity: 0
    })),
    transition('up <=> down', [
      animate('.2s ease')
    ])
  ])
};
