import {Component, OnDestroy, OnInit, Input} from '@angular/core';
import {Subscription} from 'rxjs';
import { Router } from '@angular/router';

import {Product} from '../../_models/product.types';
import {DataService} from '../../_services/data.service';

import { SwiperComponent, SwiperDirective, SwiperConfigInterface,
  SwiperScrollbarInterface, SwiperPaginationInterface } from 'ngx-swiper-wrapper';
import {Animations} from '../../animations';
import {ListProductsAnimation} from './list-products.animations';
import {Category} from '../../_models/category.types';
import {ActivatedRoute} from '@angular/router';
import {ProductsService} from '../products.service';
import {baseProto, imagesURL} from '../../_models/urls';
import {LocalizedDescription} from '../../_models/localization.types';
import {Meta, Title} from '@angular/platform-browser';
import {Language, Localization} from '../../_models/localization.types';

@Component({
  selector: 'app-list-products',
  templateUrl: './list-products.component.html',
  styleUrls: ['./list-products.component.scss'],
  animations: [ListProductsAnimation.fadeInOut, ListProductsAnimation.arrowUpDown]
})
export class ListProductsComponent implements OnInit, OnDestroy {
  //  public
  public products: Product[] = null; //  продукты
  public category: Category = null; //  текущая категория
  @Input() public relatedProducts: boolean; //  where from the component was called
  public imagePath: string = baseProto + imagesURL + '/categories/';
  public currentUrl: string = '/';
  public tempUrl: string = '/';
  public showCategoryPicture: boolean = true; // show/hide image
  public localizations: Localization[] = null; // список локализаций

  // carousel config
  public config: SwiperConfigInterface = {
    a11y: true,
    direction: 'horizontal',
    slidesPerView: 'auto',
    keyboard: true,
    mousewheel: false,
    scrollbar: false,
    navigation: true,
    pagination: false,
    spaceBetween: 50,
    watchOverflow: true,
    centerInsufficientSlides: true
  };

  // index of active element in carousel
  public index = 1;


  //  private
  private categorySub: Subscription = null;
  private productsSub: Subscription = null;
  private showCategoryPictureSub: Subscription = null;
  private localizationsSub: Subscription; //  подписка на список локализаций

  //  constructor
  constructor(
    private dataService: DataService,
    private activatedRoute: ActivatedRoute,
    private productsService: ProductsService,
    private router: Router,
    private titleService: Title,
    private meta: Meta
    ) {
      //  подписываемся на категорию
      this.categorySub = this.dataService.castCategory.subscribe((category: Category) => {
        if (category) {
          this.category = category;
          this.initMeta();
        }
      });
      //  подписываемся на продукты
      this.productsSub = this.dataService.castProducts.subscribe((products: Product[]) => {
        this.products = products;

        if (products) {
          this.index = Math.round(products.length / 2) - 1;
        }
      });
      //  подписываемся на бит отображения картинки
      this.showCategoryPictureSub = this.dataService.castShowCategoryPicture.subscribe((showCategoryPicture: boolean) => {
        this.showCategoryPicture = showCategoryPicture;
      });

      //  подписка localizations
      this.localizationsSub = this.dataService.castLocalizations.subscribe((localizations: Localization[]) => {
        this.localizations = localizations;
      });
}

  //  destroy
  public ngOnDestroy() {
    //  отписываемся от категории
    this.categorySub.unsubscribe();
    //  отписываемся от продуктов
    this.productsSub.unsubscribe();
    //  отписываемся от продуктов
    this.showCategoryPictureSub.unsubscribe();
    // отписываемся от localizations
    this.localizationsSub.unsubscribe();
  }

  // init
  ngOnInit() {
    //  делаем урл пабликом
    this.currentUrl = this.router.url;
    //  получаем идентификатор категории
    const categoryID: string = this.activatedRoute.snapshot.paramMap.get('id');
    if (categoryID) {
      console.log('getProductsByCategoryID, categoryID:', categoryID);
      this.productsService.getProductsByCategoryID(categoryID);
    } else {
      console.log('getProducts');
      this.productsService.getProducts();
    }
    this.tempUrl = this.router.url;
    // this.initMeta();

  }

  //  инициализация меты
  public initMeta() {
    //  инит переменных
    console.log('localizedSEO category: ', this.category);
    const localSEOen = this.category.localizedSEOs &&
    this.category.localizedSEOs.find((ls: LocalizedDescription) => ls.language === 'EN') ?
      this.category.localizedSEOs.find((ls: LocalizedDescription) => ls.language === 'EN') :
      {
        keywords: '',
        title: 'Grey Shop',
        description: '',
        name: ''
      };
    const localSEOru = this.category.localizedSEOs &&
    this.category.localizedSEOs.find((ls: LocalizedDescription) => ls.language === 'RU') ?
      this.category.localizedSEOs.find((ls: LocalizedDescription) => ls.language === 'RU') :
      {
        keywords: '',
        title: 'Grey Shop',
        description: '',
        name: ''
      };
    //  инит тайтла
    //  this.titleService.setTitle(localSEOen.title + ' / ' + localSEOru.title);
    this.titleService.setTitle(this.category.localizedDescriptions[0].name + ' / ' + this.category.localizedDescriptions[1].name);
    //  инит меты
    this.meta.addTag({name: 'keywords', content: localSEOen.keywords + ',' + localSEOru.keywords});
    this.meta.addTag({name: 'description', content: localSEOen.description + ',' + localSEOru.description});
    this.meta.addTag({name: 'author', content: 'Grey Shop'});
    this.meta.addTag({name: 'robots', content: 'index, follow'});


  }

    // show/hide image
    public invertShowImage() {
      this.dataService.putShowCategoryPictures(!this.showCategoryPicture);
    }
}
