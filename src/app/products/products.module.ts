import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductsRoutingModule } from './products-routing.module';
import { ListProductsComponent } from './list-products/list-products.component';
import { ProductDetailsComponent } from './product-details/product-details.component';
import { ProductCardComponent } from './product-card/product-card.component';
import { ReletedProductCardComponent } from './releted-product-card/releted-product-card.component';
import { PipesModule } from '../_pipes/pipes.module';
import {FormsModule} from '@angular/forms';


import { SwiperModule } from 'ngx-swiper-wrapper';
import { SWIPER_CONFIG } from 'ngx-swiper-wrapper';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';
import {AppModule} from '../app.module';
import {SupportModule} from '../support/support.module';
import { DirectivesModule } from '../__directives/directives.module';

// carousel config
const DEFAULT_SWIPER_CONFIG: SwiperConfigInterface = {
  direction: 'horizontal',
  slidesPerView: 1
};


@NgModule({
  declarations: [
    ListProductsComponent,
    ProductDetailsComponent,
    ProductCardComponent,
    ReletedProductCardComponent
  ],
  exports: [
    ListProductsComponent
  ],
  imports: [
    CommonModule,
    PipesModule,
    ProductsRoutingModule,
    SwiperModule,
    FormsModule,
    SupportModule,
    DirectivesModule
  ],
  providers: [
    {
      provide: SWIPER_CONFIG,
      useValue: DEFAULT_SWIPER_CONFIG
    }
  ]
})
export class ProductsModule { }
