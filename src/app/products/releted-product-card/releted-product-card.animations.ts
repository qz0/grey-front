import { trigger, state, style, transition, animate } from '@angular/animations';

export const ProductCardAnimations = {
 // показать / скрыть back картинку
  showHideBackImage: trigger('showHideBackImage', [
    transition(':enter', [
      style({ opacity: 0}),
      animate('.3s ease', style({ opacity: 1})),
    ]),
    transition(':leave', [
      animate('.3s ease', style({ opacity: 0}))
    ])
  ])
};
