import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReletedProductCardComponent } from './releted-product-card.component';

describe('ReletedProductCardComponent', () => {
  let component: ReletedProductCardComponent;
  let fixture: ComponentFixture<ReletedProductCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReletedProductCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReletedProductCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
