import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {ProductCardAnimations} from './releted-product-card.animations';
import {CartService} from '../../cart/cart.service';
import {Cart} from '../../_models/cart.types';
import {Product} from '../../_models/product.types';
import {Subscription} from 'rxjs';
import {DataService} from '../../_services/data.service';
import {GenerateService} from '../../_services/generate.service';
import { Router } from '@angular/router';
import {baseProto, imagesURL} from '../../_models/urls';


@Component({
  selector: 'app-releted-product-card',
  animations: [ProductCardAnimations.showHideBackImage],
  templateUrl: './releted-product-card.component.html',
  styleUrls: ['./releted-product-card.component.scss']
})
export class ReletedProductCardComponent implements OnInit, OnDestroy {
  // public
  @Input() product: Product = null;  // product
  public cart: Cart = null; // cart
  public pathToGo: string = '/product/products/'; //  ссылка на продукт
  public imagePath: string = baseProto + imagesURL + '/products/';
  public optionsImagePath: string = baseProto + imagesURL;
  public showBackImage: boolean = false; // show/hide add to cart button

  // private
  private cartSub: Subscription = null;

  constructor(private cartService: CartService, private dataService: DataService, private router: Router) {

    // subscription
    this.cartSub = this.dataService.castCart.subscribe((cart: Cart) => {
      if (localStorage.getItem('cart')) {
        this.cart = JSON.parse(localStorage.getItem('cart'));
      } else {
        this.cart = cart;
      }
    });
  }

  // destroy
  ngOnDestroy() {
    this.cartSub.unsubscribe();
  }

  // init
  ngOnInit() {
    this.pathToGo = '/products/product/' + String(this.product.id);
    // console.log('current cart', this.cart);
  }

  //  перейти к карточке продукта
  public showProductDetail() {
    if (this.product) {
      // console.log('Current route: ', String(this.product.id));
      // console.log('Current product: ', this.product);
      this.router.navigate(['/products/product/' + String(this.product.id)]);
    }
  }

  showImage() {
    this.showBackImage = true;
  }

  hideImage() {
    this.showBackImage = false;
  }
}
