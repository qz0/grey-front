import { trigger, state, style, transition, animate } from '@angular/animations';

export const ProductDetailsAnimations = {
  // Переключение страниц с отзывами
  switchReviews: trigger('switchReviews', [
    transition(':enter', [
      style({ opacity: 0, transform: 'scale(0.5)' }),
      animate('.3s', style({ opacity: 1, transform: 'translateX(0)' })),
    ])
  ]),
  moveToCart: trigger('moveToCart', [
    state('true', style({
      'z-index': 100000,
      transform: 'translate({{x}}px,-{{y}}px) scale(0.1)'
    }),  {params: {x: 1, y: 1}}),
    state('false', style({
      transform: 'translate(0, 0)'
    }),  {params: {x: 1, y: 1}}),
    transition('false => true', animate('500ms  ease-in'))
  ]),
};
