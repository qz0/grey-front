import {Component, OnDestroy, OnInit, ElementRef, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Meta, Title} from '@angular/platform-browser';
import {Subscription} from 'rxjs';

import {ProductDetailsAnimations} from './product-details.animation';
import {Product} from '../../_models/product.types';
import {ProductsService} from '../products.service';
import {DataService} from '../../_services/data.service';
import {OptionType, OptionValue} from '../../_models/option.types';
import {baseProto, imagesURL} from '../../_models/urls';
import {LocalizedDescription} from '../../_models/localization.types';
import {Localization} from '../../_models/localization.types';
import {Cart} from '../../_models/cart.types';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.scss'],
  animations: [ProductDetailsAnimations.switchReviews, ProductDetailsAnimations.moveToCart]
})
export class ProductDetailsComponent implements OnInit, OnDestroy {
  // картинка продукта для анимации на десктопе
  @ViewChild('imageForAnimationDesktop', {static: false}) imageForAnimationDesktop: ElementRef;
  // картинка продукта для анимации на мобильном
  @ViewChild('imageForAnimationMobile', {static: false}) imageForAnimationMobile: ElementRef;
  // поле для ввода количества товара
  @ViewChild('productCountInput', {static: false}) productCountInput: ElementRef;

  //  public
  public product: Product = null; //  продукт
  public numberOfPage: number = 1; //  номер открытой страницы с отзывами
  public optionValues: OptionValue[] = null;  //  список опций продукта
  public amountOfProduct: number = 1; //  количество продукта на странице
  public selectedColorId: number = null; // id выбранной опция цвета
  public colorWarning: boolean = false; // показать предупреждение, что не выбран цвет
  public selectedSizeType: string = 'int'; // выбранный тип рарзмера продукта
  public selectedSizeId: number; // id выбранной опция размера
  public sizeWarning: boolean = false; // показать предупреждение, что не выбран размер
  public selectedOptionValues: OptionValue[] = []; // выбранные опции
  public imagePath: string = baseProto + imagesURL + '/products/';
  public isDisabledAddToCart: boolean = true; //  блокировать кнопку добавления в корзину
  public optionsImagePath: string = baseProto + imagesURL; // путь к картинке опции
  public frontImagePath: string = this.imagePath + 'none.jpg'; // путь к FronrImage
  public moveToCartAnimation: boolean = false; // анимация перемещения товара в корзину
  public imagePositionDesktop = {x: null, y: null}; // координаты картинки для анимации относительно окна браузера на десктопе
  public imagePositionMobile = {x: null, y: null}; // координаты картинки для анимации  относительно окна браузера на мобильном
  public localizations: Localization[] = null; // список локализаций
  public cart: Cart = null; // список локализаций

  //  TEST
  public optionValueForTest: OptionValue = null;  //  option value for test

  //  private
  private productSub: Subscription;
  private cartSub: Subscription;
  private localizationsSub: Subscription; //  подписка на список локализаций

  //  constructor
  constructor(private activatedRoute: ActivatedRoute,
              private productsService: ProductsService,
              private dataService: DataService,
              private titleService: Title,
              private meta: Meta) {
    //  подписываемся на асинхроны
    this.productSub = this.dataService.castProduct.subscribe((product: Product) => {
      if (product) {
        this.product = product;
        if (this.product.imageFront.url) {
          this.frontImagePath = this.imagePath + this.product.imageFront.url + '.jpg';
        }
        this.initMeta();
        //  нет опций - сразу доступно добавление в корзину
        this.isDisabledAddToCart = Boolean(product.productOptions.length);

        console.log('ProductOptions: ', this.product.productOptions.length);
      }
    });

    // подписка на изменение url и получение нового продукта при изменении url
    this.activatedRoute.params.subscribe(params => {
      if (params) {
        this.getProductsById();
      }
    });

    //  подписка localizations
    this.localizationsSub = this.dataService.castLocalizations.subscribe((localizations: Localization[]) => {
      this.localizations = localizations;
    });

    //  подписка cart
    this.cartSub = this.dataService.castCart.subscribe((cart: Cart) => {
      // console.log('new cart: ', cart);
      this.cart = cart;
    });
}

  //  destoyer
  ngOnDestroy() {
    //  отписываемся от подписок
    this.productSub.unsubscribe();
    this.cartSub.unsubscribe();
    this.localizationsSub.unsubscribe();
  }

  //  initor
  ngOnInit() {
    this.getProductsById();
  }

  public getProductsById() {
      //  получаем id
      const productID = this.activatedRoute.snapshot.paramMap.get('id');
      //  если передали id
      if (productID) {
        this.productsService.getProductByID(productID);
      } else {
        this.productsService.getProductByID(null);
      }
  }

  // изменить страницу с отзывами
  public choosePageOfReviews(num: number) {
    this.numberOfPage = num;
  }

  //  добавить товар в корзину
  public addProductToCart() {

    // все ли опции выбраны
    let isAllOptionSelected = true;

    // проверка - выбраны ли опции
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < this.product.productOptions.length; i++) {

      if (this.product.productOptions[i].option.type.id === 1 && !this.selectedColorId) {
        this.colorWarning = true;
        isAllOptionSelected = false;
      }

      if (this.product.productOptions[i].option.type.id === 2 && !this.selectedSizeId) {
        this.sizeWarning = true;
        isAllOptionSelected = false;
      }
    }

    // если не все опции выбраны и количество товара меньше единицы - прервать выполнение функции
    if (!isAllOptionSelected || this.amountOfProduct < 1) {
      return;
    }
    // console.log('product details cart: ', this.cart);
    const newOptionValues: OptionValue[] = this.selectedOptionValues.map(selectedOptionValue => Object.assign({}, selectedOptionValue));

    this.productsService.addProductToCart(this.product, newOptionValues, this.amountOfProduct);

    this.startImageToCartAnimation();
  }

  // управление анимацией полета продукта в корзину
  public startImageToCartAnimation() {
     // расчет позиции картинки для анимации относительно окна браузера на десктопе
    this.imagePositionDesktop.x = Math.round(
      document.documentElement.clientWidth -
      this.imageForAnimationDesktop.nativeElement.getBoundingClientRect().right);
    this.imagePositionDesktop.y = Math.round(this.imageForAnimationDesktop.nativeElement.getBoundingClientRect().top);

    // расчет позиции картинки для анимации относительно окна браузера на мобильном
    this.imagePositionMobile.x = Math.round(
      document.documentElement.clientWidth -
      this.imageForAnimationMobile.nativeElement.getBoundingClientRect().right);
    this.imagePositionMobile.y = Math.round(this.imageForAnimationMobile.nativeElement.getBoundingClientRect().top);

    // включение анимации
    this.moveToCartAnimation = true;
    // картинка в исходное положение
    setTimeout(() => this.moveToCartAnimation = false, 600);
  }

  // поменять путь front image
  public changeFrontImagePath(path: string) {
    this.frontImagePath = path;
  }

  //  изменить опции продукта
  public changeProductOptions(optionType: OptionType, optionValue: OptionValue) {
    // console.log('changeOption before before enabled cart - ', this.cart);
    // console.log('Выбрали опцию: ', optionValue, optionType);
    //  ищем опции во временном хранилище
    if (this.selectedOptionValues.length > 0) {
      const index = this.selectedOptionValues
        .findIndex((selectedOptionValue: OptionValue) => selectedOptionValue.optionTypeID === optionType.id);
      //  грохаем эту опцию
      if (index !== -1) {
        this.selectedOptionValues.splice(index, 1);
      }
    }
    //  записываем во временное поле идентификатор типа
    optionValue.optionTypeID = optionType.id;
    optionValue.sizeType = this.selectedSizeType;

    //  добавляем выбранную опцию во временный массив
    //  пушим клон, чтобы избежать магии с динамическим изменением опций в корзине
    this.selectedOptionValues.push(Object.assign({}, optionValue));
    // this.enableAddToCart();


    if (optionType.name === 'color') {
      this.selectedColorId = optionValue.id;
      this.colorWarning = false;
    }
    if (optionType.name === 'size') {
      this.selectedSizeId = optionValue.id;
      this.sizeWarning = false;
    }

    // console.log('changeOption before enabled cart - ', this.cart);
    //  передергиваем видимость кнопки корзины
    this.enableAddToCart();

    // console.log('changeOption after enabled cart - ', this.cart);
  }

  // изменить тип размера продукта
  public selectSizeType(sizeType: string) {
    this.selectedSizeType = sizeType;
  }

  //  уменьшить количество товара
  public decAmountOfProduct() {
    //  уменьшили количество товара
    this.amountOfProduct--;
    //  если ушло в отрицательные принудительно сделали ноль
    if (this.amountOfProduct < 0) {
      this.amountOfProduct = 0;
    }
  }

  //  активировать кнопку добавить в корзину
  public enableAddToCart() {
    //  если выбраны все опции
    if (this.product && this.product.productOptions && this.product.productOptions.length === this.selectedOptionValues.length) {
      // выбраны все опции - активируем добавление товара в корзину
      this.isDisabledAddToCart = false;
    }
  }

  // раскрыть свернутый комментарий
  public expandCommentText(event) {
    const clickedElement = event.target || event.srcElement;
    const reviewText = clickedElement.parentElement.querySelector('.comment-text');

    clickedElement.classList.add('hide');
    reviewText.classList.remove('comment-text_hide');
  }

  //  увеличили количество товара
  public incAmountOfProduct() {
    //  увеличили количество товара
    this.amountOfProduct++;

    if (this.amountOfProduct > 999) {
      this.amountOfProduct = 999;
    }
  }

  // отсекает ввод букв в поле
  public checkOnSymbols(event) {
    const key = Number(event.key);
    // прерывать если введено не число кроме Backspace
    if ((isNaN(key) || event.key === null) && event.key !== 'Backspace') {
      event.preventDefault();
      return;
    }

    // если есть четыре символа прерывать ввод новых кроме Backspace
    if ( this.amountOfProduct.toString().length === 4 && event.key !== 'Backspace') {
      event.preventDefault();
    }
  }

  // изменение количества товра вручную
  public changeAmountOfProduct() {

    // удалены все символы - подставлять ноль
    if (this.productCountInput.nativeElement.value.length === 0) {
      this.amountOfProduct = 0;
      return;
    }

    // если введенная величина больше трех симоволов - сбрасывать до 999
    if (this.productCountInput.nativeElement.value.length > 3) {
      this.amountOfProduct = 999;
      return;
    }

    // присвоить введенное значение
    this.amountOfProduct = parseInt(this.productCountInput.nativeElement.value, 10);
  }

  //  инициализация меты
  public initMeta() {
    //  инит переменных
    const localSEOen = this.product.localizedSEOs &&
    this.product.localizedSEOs.find((ls: LocalizedDescription) => ls.language === 'EN') ?
      this.product.localizedSEOs.find((ls: LocalizedDescription) => ls.language === 'EN') :
      {
        keywords: '',
        title: 'Grey Shop',
        description: '',
        name: ''
      };
    const localSEOru = this.product.localizedSEOs &&
    this.product.localizedSEOs.find((ls: LocalizedDescription) => ls.language === 'RU') ?
      this.product.localizedSEOs.find((ls: LocalizedDescription) => ls.language === 'RU') :
      {
        keywords: '',
        title: 'Grey Shop',
        description: '',
        name: ''
      };
    //  инит тайтла

    //  TODO: починить
    this.titleService.setTitle(this.product.localizedDescriptions[0].name + ' / ' + this.product.localizedDescriptions[1].name);
    // this.titleService.setTitle(localSEOen.name + ' / ' + localSEOru.name);
    //  инит меты
    this.meta.addTag({name: 'keywords', content: localSEOen.keywords + ',' + localSEOru.keywords});
    this.meta.addTag({name: 'description', content: localSEOen.description + ',' + localSEOru.description});
    this.meta.addTag({name: 'author', content: 'Grey Shop'});
    this.meta.addTag({name: 'robots', content: 'index, follow'});
  }

  // переключить активную кнопку выбора стараницы с отзывами
  public onButtonGroupClick(event) {
    const clickedElement = event.target || event.srcElement;

    if (clickedElement.nodeName === 'LI') {

      const isCertainButtonAlreadyActive = clickedElement.parentElement.querySelector('.active');
      // if a Button already has Class: .active
      if (isCertainButtonAlreadyActive) {
        isCertainButtonAlreadyActive.classList.remove('active');
      }

      clickedElement.className += ' ' + 'active';
    }
  }
}
