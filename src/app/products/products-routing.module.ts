import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListProductsComponent } from './list-products/list-products.component';
import { ProductDetailsComponent } from './product-details/product-details.component';


export const productsRoutes: Routes = [
  {
    //  отдельный product
    path: 'product', component: ProductDetailsComponent
  },
  {
    //  отдельный product
    path: 'product/:id', component: ProductDetailsComponent
  },
  {
    //  отдельный product
    path: 'category/:id', component: ListProductsComponent
  },
  {
    //  все products
    path: '', component: ListProductsComponent
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(productsRoutes),
    RouterModule.forRoot(productsRoutes, { scrollPositionRestoration: 'enabled' })
  ],
  exports: [RouterModule]
})
export class ProductsRoutingModule { }
