import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {ProductCardAnimations} from './product-card.animations';
import {CartService} from '../../cart/cart.service';
import {Cart} from '../../_models/cart.types';
import {Product} from '../../_models/product.types';
import {Subscription} from 'rxjs';
import {DataService} from '../../_services/data.service';
import {GenerateService} from '../../_services/generate.service';
import { Router } from '@angular/router';
import {baseProto, imagesURL} from '../../_models/urls';
import { ProductDetailsAnimations } from '../product-details/product-details.animation';
import {Language, Localization} from '../../_models/localization.types';


@Component({
  selector: 'app-product-card',
  animations: [ProductCardAnimations.showHideAddToCartButton, ProductCardAnimations.showHideBackImage],
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.scss']
})
export class ProductCardComponent implements OnInit, OnDestroy {
  // public
  @Input() product: Product = null;  // product
  public showAddToCart: boolean = false; // show/hide add to cart button
  public cart: Cart = null; // cart
  public pathToGo: string = '/product/products/'; //  ссылка на продукт
  public imagePath: string = baseProto + imagesURL + '/products/small/';
  public optionsImagePath: string = baseProto + imagesURL;
  public localizations: Localization[] = null; // список локализаций

  // private
  private cartSub: Subscription = null;
  private localizationsSub: Subscription; //  подписка на список локализаций

  constructor(private cartService: CartService, private dataService: DataService, private router: Router) {

    // subscription
    this.cartSub = this.dataService.castCart.subscribe((cart: Cart) => {
      if (localStorage.getItem('cart')) {
        this.cart = JSON.parse(localStorage.getItem('cart'));
      } else {
        this.cart = cart;
      }
    });

    //  подписка localizations
    this.localizationsSub = this.dataService.castLocalizations.subscribe((localizations: Localization[]) => {
      this.localizations = localizations;
    });
}

  // destroy
  ngOnDestroy() {
    this.cartSub.unsubscribe();
    this.localizationsSub.unsubscribe();
  }

  // init
  ngOnInit() {
    this.pathToGo = '/products/product/' + String(this.product.id);
    // console.log('current cart', this.cart);
  }

  // add product to card
  public addProductToCart(product: Product) {
    // console.log('Create cart and add product to card');
    event.stopPropagation();
    const newCartItem = GenerateService.generateCartItem(product);

    const localCart = JSON.parse(localStorage.getItem('cart'));
    localCart.cartItems.push(newCartItem);
    this.cart = localCart;

    localStorage.setItem('cart', JSON.stringify(this.cart));

    this.cartService.addProductToCart(product, []);
  }

  // hide button
  public hideButton() {
    this.showAddToCart = false;
  }

  // show showButton
  public showButton() {
    this.showAddToCart = true;
  }

  //  перейти к карточке продукта
  public showProductDetail() {
    if (this.product) {
      // console.log('Current route: ', String(this.product.id));
      // console.log('Current product: ', this.product);
      this.router.navigate(['/products/product/' + String(this.product.id)]);
    }
  }
}
