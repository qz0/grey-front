import { trigger, state, style, transition, animate } from '@angular/animations';

export const ProductCardAnimations = {
  // показать / скрыть кнопку add to cart и картинку
  showHideAddToCartButton: trigger('showHideAddToCartButton', [
    transition(':enter', [
      style({ opacity: 0, transform: 'translateY(-12.5rem)' }),
      animate('.3s ease', style({ opacity: 1, transform: 'translateY(0)' })),
    ]),
    transition(':leave', [
      animate('.3s ease', style({ opacity: 0, transform: 'translateY(-12.5rem)' }))
    ])
  ]),
  // показать / скрыть back картинку
  showHideBackImage: trigger('showHideBackImage', [
    transition(':enter', [
      style({ opacity: 0}),
      animate('.3s ease', style({ opacity: 1})),
    ]),
    transition(':leave', [
      animate('.3s ease', style({ opacity: 0}))
    ])
  ])
};
