import { Injectable } from '@angular/core';
import {DataService} from '../_services/data.service';
import {Product} from '../_models/product.types';
import {OptionValue} from '../_models/option.types';
import {Category} from '../_models/category.types';
import {Cart} from '../_models/cart.types';
import {Subscription} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {
  //  public
  public cart: Cart = null; //  экземпляр корзины

  //  private
  // private cartSub = Subscription; //  cart subscription

  constructor(private dataService: DataService) {
    // this.cartSub = this.dataService.castCart.subscribe((cart: Cart) => {
    //   if (cart) {
    //     this.cart = cart;
    //   }
    // });
  }

  //  добавить продукт в корзину
  public addProductToCart(product: Product, optionValues: OptionValue[], amountOfOptions: number) {
    // console.log('добавлен товар со следующим содержанием: ', product, optionValues, amountOfOptions);
    //  дергаем сервис
    this.dataService.addProductToCart(product, optionValues, amountOfOptions);
  }

  // инитим продукты
  public getCategoryByID(categoryID?: string) {
    // дергаем сервис данных
    this.dataService.getCategoryByID(categoryID);
  }

  // инитим продукты
  public getProductsByCategoryID(categoryID?: string) {
    // дергаем сервис данных
    this.dataService.getProductsByCategoryID(categoryID);
  }

  // инитим продукты
  public getProductByID(productID?: string) {
    // дергаем сервис данных
    this.dataService.getProductByID(productID);
  }

  // инитим продукты
  public getProducts(category?: Category) {
    // дергаем сервис данных
    if (category) {
      this.dataService.getProductsByCategory(category);
    } else {
      this.dataService.getProducts();
    }
  }


}
