// animations.ts
import { trigger, state, style, transition, animate } from '@angular/animations';

export const Animations = {
  // универсальная анимация раскрытия блоков
  fadeInOut: trigger('fadeInOut', [
    transition(':enter', [
      style({ opacity: 0, height: 0 }),
      animate('.2s ease', style({ opacity: 1, height: '*' })),
    ]),
    transition(':leave', [
      animate('.2s ease', style({ opacity: 0, height: 0 }))
    ])
  ]),
  // открыть/закрыть меню
  showHideMenu: trigger('showHideMenu', [
    transition(':enter', [
      style({ height: 0 }),
      animate('.3s', style({ height: '*' })),
    ]),
    transition(':leave', [
      style({ height: '*' }),
      animate('.3s', style({ height: 0 }))
    ])
  ])
};
