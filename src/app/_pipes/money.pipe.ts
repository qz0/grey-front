import {OnDestroy, Pipe, PipeTransform} from '@angular/core';

import {Subscription} from 'rxjs';
import {DataService} from '../_services/data.service';
import {Currency} from '../_models/currency.types';
import {CurrencyService} from '../_services/currency.service';

@Pipe({
  name: 'money',
  pure: false
})

//  конвертация ы конкретный язык
export class MoneyPipe implements PipeTransform, OnDestroy {
  // public
  public currency: Currency = null;

  //  private
  private currencySub: Subscription;

  //  constructor
  constructor(private dataService: DataService, private currencyService: CurrencyService) {
    //  подписываемся на currency
    this.currencySub = this.dataService.castCurrency.subscribe((currency: Currency) => {
      if (currency) {
        // console.log('Have changes :', language);
        this.currency = currency;
      }
    });
  }

  //  destroyer
  ngOnDestroy() {
    //  unsubs
    this.currencySub.unsubscribe();
  }

  //  встроенный интерфейс для конвертации
  public transform(cost: number): string {
    //  если все передали
    return this.currencyService.convertToMoney(cost);
    // if (count != null && this.currency) {
    //   //  если символ перед числом
    //   if (this.currency.before) {
    //     return this.currency.symbol + '' + String(Math.ceil(count / this.currency.multiplier));
    //   } else {
    //     //  символ сзади
    //     return String(Math.ceil(count / this.currency.multiplier)) + '' + this.currency.symbol;
    //   }
    // } else {
    //   //  возвращаем просто строку без символа валюты
    //   return String(Math.ceil(count / this.currency.multiplier));
    // }
  }
}
