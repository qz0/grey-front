import {OnDestroy, Pipe, PipeTransform} from '@angular/core';

import {Language, Localization, LocalizedDescription} from '../_models/localization.types';
import {Subscription} from 'rxjs';
import {DataService} from '../_services/data.service';

@Pipe({
  name: 'locale_keyword',
  pure: false
})

//  конвертация ы конкретный язык
export class LocaleKeywordPipe implements PipeTransform, OnDestroy {
  // public
  public languageString: string = 'EN';
  // public localizations: Localization[] = null;

  //  private
  private languageSub: Subscription;
  // private localizationsSub: Subscription;

  //  constructor
  constructor(private dataService: DataService) {
    //  подписываемся на язык
    this.languageSub = this.dataService.castLanguage.subscribe((language: Language) => {
      if (language) {
        this.languageString = language.language;
      }
    });
    //  подписываемся на локализации
    // this.localizationsSub = this.dataService.castLocalizations.subscribe((localizations: Localization[]) => {
    //   if (localizations) {
    //     this.localizations = localizations;
    //   }
    // });
  }

  //  destroyer
  ngOnDestroy() {
    this.languageSub.unsubscribe();
    // this.localizationsSub.unsubscribe();
  }

  //  встроенный интерфейс для конвертации
  public transform(localizations: Localization[], keyword: string): string {
    //  если что-то передали
    if (this.languageString && keyword && localizations) {
      //  нашли искомый ключ для локализации
      const localization: Localization = localizations.find(item => item.keyword === keyword);
      //  выбрали язык
      const localText = localization.localizedTexts.find(locale => locale.language === this.languageString);

      if (localText) {
        //  если есть вернули найденное, если нет ноне
        return localText.text;
      }
    }
    return 'none';
  }
}
