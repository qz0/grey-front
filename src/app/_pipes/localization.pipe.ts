import {OnDestroy, OnInit, Pipe, PipeTransform} from '@angular/core';

import {Language, LocalizedDescription} from '../_models/localization.types';
import {Subscription} from 'rxjs';
import {DataService} from '../_services/data.service';
import {Cart} from '../_models/cart.types';

@Pipe({
  name: 'localization',
  pure: false
})

//  конвертация ы конкретный язык
export class LocalizationPipe implements PipeTransform, OnDestroy {
  // public
  public languageString: string = null;

  //  private
  private languageSub: Subscription;

  //  constructor
  constructor(private dataService: DataService) {
    //
    // if (!localStorage.getItem('language')) {
    //   this.languageString = 'EN';
    // } else {
    //   const language: Language = JSON.parse(localStorage.getItem('language'));
    //   this.languageString = language.language;
    // }
    //  подписываемся на язык
    this.languageSub = this.dataService.castLanguage.subscribe((language: Language) => {
      if (language) {
        this.languageString = language.language;
      }
    });
  }

  //  destroyer
  ngOnDestroy() {
    this.languageSub.unsubscribe();
  }

  //  встроенный интерфейс для конвертации
  public transform(multilanguageItem: LocalizedDescription[]): LocalizedDescription {
    // console.log('locale!');
    if (multilanguageItem) {
      const singleLanguageItem: LocalizedDescription = multilanguageItem.find(item => item.language === this.languageString);
      return singleLanguageItem;
    } else {
      return {
        language: 'RU',
        title: 'заоголовк отсутствует',
        name: 'имя отсутствует',
        description: 'описание отсутствует',
        keywords: ''
      };
    }
  }
}
