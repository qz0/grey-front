import { NgModule } from '@angular/core';
import {CommonModule} from '@angular/common';

import {LocalizationPipe} from './localization.pipe';
import {LocalizationArrayPipe} from './localization-array.pipe';
import {MoneyPipe} from './money.pipe';
import {SafePipe} from './safe.pipe';
import {CurrencyPipe} from './currency.pipe';
import {LocaleKeywordPipe} from './locale-keyword.pipe';

@NgModule({
  declarations: [
    CurrencyPipe,
    LocaleKeywordPipe,
    LocalizationPipe,
    LocalizationArrayPipe,
    MoneyPipe,
    SafePipe
  ],
  imports: [
    CommonModule
  ],
  exports: [
    CurrencyPipe,
    LocaleKeywordPipe,
    LocalizationPipe,
    LocalizationArrayPipe,
    MoneyPipe,
    SafePipe
  ]
})
export class PipesModule { }
