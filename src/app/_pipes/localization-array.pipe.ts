import {OnDestroy, Pipe, PipeTransform} from '@angular/core';

import {Language, LocalizedDescription, LocalizedText} from '../_models/localization.types';
import {Subscription} from 'rxjs';
import {DataService} from '../_services/data.service';

@Pipe({
  name: 'localization_array',
  pure: false
})

//  конвертация ы конкретный язык
export class LocalizationArrayPipe implements PipeTransform, OnDestroy {
  // public
  public languageString: string = 'EN';

  //  private
  private languageSub: Subscription;

  //  constructor
  constructor(private dataService: DataService) {

    // if (!localStorage.getItem('language')) {
    //   this.languageString = 'EN';
    // } else {
    //   const language: Language = JSON.parse(localStorage.getItem('language'));
    //   this.languageString = language.language;
    // }
    //  подписываемся на язык
    this.languageSub = this.dataService.castLanguage.subscribe((language: Language) => {
      if (language) {
        console.log('Have changes :', language);
        this.languageString = language.language;
      }
    });
  }

  //  destroyer
  ngOnDestroy() {
    this.languageSub.unsubscribe();
  }

  //  встроенный интерфейс для конвертации
  public transform(multilanguageItems: LocalizedText[]): LocalizedText[] {
    // console.log('locale!');
    if (multilanguageItems) {
      const singleLanguageItems: LocalizedText[] = multilanguageItems.filter(item => item.language === this.languageString);
      return singleLanguageItems;
    } else {
      return [];
    }
  }
}
