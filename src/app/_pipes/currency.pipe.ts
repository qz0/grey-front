import {OnDestroy, Pipe, PipeTransform} from '@angular/core';

import {Subscription} from 'rxjs';
import {DataService} from '../_services/data.service';
import {Currency} from '../_models/currency.types';
import {CurrencyService} from '../_services/currency.service';

@Pipe({
  name: 'currency',
  pure: false
})

//  конвертация ы конкретный язык
export class CurrencyPipe implements PipeTransform {

  //  constructor
  constructor(private currencyService: CurrencyService) {
  }

  //  встроенный интерфейс для конвертации
  public transform(cost: number): number {
      //  возвращаем округленное вверх число в пересчитанной валюте
      return this.currencyService.convertToCurrency(cost);
  }
}
