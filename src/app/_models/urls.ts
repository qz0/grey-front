//  описание путей
//  base url api
export const baseProto = 'https://';
// export const baseProto = 'http://';
export const baseUrl = 'admin.grey-line.com:9000';
// export const baseUrl = 'localhost:7000';
export const apiURL = '/api/v1';
export const imagesURL = 'tarkov.grey-line.com/static';

//  ADDRESSES
export const urlAddress = {
  //  add address
  create: '/addresses/create',
  //  get all addresses
  readAll: '/addresses/read_all',
  //  get current address
  readCurrent: '/addresses/read_current',
  //  change address
  update: '/addresses/update',
  //  delete address
  delete: '/addresses/delete',
};

//  ASSEMBLIES
export const urlAssembly = {
  //  add assembly
  create: '/assemblies/create',
  //  get all assemblies
  readAll: '/assemblies/read_all',
  //  get current assembly
  readCurrent: '/assemblies/read_current',
  //  change assembly
  update: '/assemblies/update',
  //  delete assembly
  delete: '/assemblies/delete',
};

//  AUTHENTICATE
//  логин
export const urlAuth = {
  //  logIn
  signIn: '/authenticate/sign_in',
  //  logOut
  signOut: '/authenticate/sign_out',
};

//  BRANDS
export const urlBrand = {
  //  add brand
  create: '/brands/create',
  //  get all brands
  readAll: '/brands/read_all',
  //  get current brand
  readCurrent: '/brands/read_current',
  //  change brand
  update: '/brands/update',
  //  delete brand
  delete: '/brands/delete',
};

//  BROCKER
export const urlBrocker = {
  //  get content
  readContent: '/brocker/read_content',
};

//  CARTS
export const urlCart = {
  //  add cart
  create: '/carts/create',
  //  get all cart
  readAll: '/carts/read_all',
  //  get current cart
  readCurrent: '/carts/read_current',
  //  change cart
  update: '/carts/update',
  //  delete cart
  delete: '/carts/delete',
  //  add cartItem to cart
  addProduct: '/carts/add_product',
  //  delete cartItem to cart
  deleteProduct: '/carts/delete_product',
  //  delete cartItem to cart
  deleteAllInProduct: '/carts/delete_all_in_product',
};

//  CATEGORIES
export const urlCategory = {
  //  add category
  create: '/categories/create',
  //  get all categories
  readAll: '/categories/read_all',
  //  get current category
  readCurrent: '/categories/read_current',
  //  get top categories
  readTop: '/categories/read_top',
  //  change category
  update: '/categories/update',
  //  delete category
  delete: '/categories/delete',
};

//  COUNTRIES
export const urlCountry = {
  //  add countries
  create: '/countries/create',
  //  get all countries
  readAll: '/countries/read_all',
  //  get all countries
  readAllShort: '/countries/read_all_short',
  //  get current countries
  readCurrent: '/countries/read_current',
  //  change countries
  update: '/countries/update',
  //  delete countries
  delete: '/countries/delete',
};

//  CURRENCIES
export const urlCurrency = {
  //  add currency
  create: '/currencies/create',
  //  get all currencies
  readAll: '/currencies/read_all',
  //  get current currency
  readCurrent: '/currencies/read_current',
  //  change currency
  update: '/currencies/update',
  //  delete currency
  delete: '/currencies/delete',
};

// CUSTOMERS
export const urlCustomer = {
  //  add customer
  create: '/customers/create',
  //  get all customers
  readAll: '/customers/read_all',
  //  get current customer
  readCurrent: '/customers/read_current',
  //  change customer
  update: '/customers/update',
  //  delete customer
  delete: '/customers/delete',
};

// CUSTOMER'S RANKS
export const urlCustomerRank = {
  //  add customer rank
  create: '/customer_ranks/create',
  //  get all customer ranks
  readAll: '/customer_ranks/read_all',
  //  get current customer rank
  readCurrent: '/customer_ranks/read_current',
  //  change customer rank
  update: '/customer_ranks/update',
  //  delete customer rank
  delete: '/customer_ranks/delete',
};

//  DELIVERIES
export const urlDelivery = {
  //  add delivery
  create: '/deliveries/create',
  //  get all deliveries
  readAll: '/deliveries/read_all',
  //  get current delivery
  readCurrent: '/deliveries/read_current',
  //  change delivery
  update: '/deliveries/update',
  //  delete delivery
  delete: '/deliveries/delete',
  //  change priority delivery
  changePriority: '/deliveries/change_priority',
  //  change priority delivery
  getCost: '/deliveries/get_cost',
};

//  IMAGES
export const urlImage = {
  //  add image
  create: '/images/create',
  //  get all images
  readAll: '/images/read_all',
  //  get current image
  readCurrent: '/images/read_current',
  //  change image
  update: '/images/update',
  //  delete image
  delete: '/images/delete',
  //  upload image
  upload: '/images/upload',
};

//  ITEMS
export const urlItem = {
  //  add item
  create: '/items/create',
  //  get all items
  readAll: '/items/read_all',
  //  get current item
  readCurrent: '/items/read_current',
  //  change item
  update: '/items/update',
  //  delete item
  delete: '/items/delete',
};

//  ITEM SHADOWS
export const urlItemShadow = {
  //  add item shadow
  create: '/item_shadows/create',
  //  get all items shadow
  readAll: '/item_shadows/read_all',
  //  get current item shadow
  readCurrent: '/item_shadows/read_current',
  //  change item shadow
  update: '/item_shadows/update',
  //  delete item shadow
  delete: '/item_shadows/delete',
};

//  LANGUAGES
export const urlLanguage = {
  //  add language
  create: '/languages/create',
  //  get all languages
  readAll: '/languages/read_all',
  //  get current language
  readCurrent: '/languages/read_current',
  //  change language
  update: '/languages/update',
  //  delete language
  delete: '/languages/delete',
};

//  LOCALIZATIONS
export const urlLocalization = {
  // //  add language
  // create: '/languages/create',
  //  get all languages
  readAll: '/localizations/read_all',
  // //  get current language
  // readCurrent: '/languages/read_current',
  // //  change language
  // update: '/languages/update',
  // //  delete language
  // delete: '/languages/delete',
};

//  MANUFACTURERS
export const urlManufacturer = {
  //  add manufacturer
  create: '/manufacturers/create',
  //  get all manufacturers
  readAll: '/manufacturers/read_all',
  //  get current manufacturer
  readCurrent: '/manufacturers/read_current',
  //  change manufacturer
  update: '/manufacturers/update',
  //  delete manufacturer
  delete: '/manufacturers/delete',
};

//  OPTIONS
export const urlOption = {
  //  add option
  create: '/options/create',
  //  get all options
  readAll: '/options/read_all',
  //  get current option
  readCurrent: '/options/read_current',
  //  change option
  update: '/options/update',
  //  delete option
  delete: '/options/delete',
};

//  OPTION TYPES
export const urlOptionType = {
  //  add option type
  // create: '/option_types/create',
  //  get all option types
  readAll: '/option_types/read_all',
  //  get current option type
  // readCurrent: '/option_types/read_current',
  //  change option type
  // update: '/option_types/update',
  //  delete option type
  // delete: '/option_types/delete',
};

//  OPTION VALUES
export const urlOptionValue = {
  //  add option value
  create: '/option_values/create',
  //  get all option values
  readAll: '/option_values/read_all',
  //  get current option value
  readCurrent: '/option_values/read_current',
  //  change option value
  update: '/option_values/update',
  //  delete option value
  delete: '/option_values/delete',
};

//  OPTIONS SETS
export const urlOptionsSet = {
  //  add options set
  create: '/options_sets/create',
  //  get all options sets
  readAll: '/options_sets/read_all',
  //  get current options set
  readCurrent: '/options_sets/read_current',
  //  change options set
  update: '/options_sets/update',
  //  delete options set
  delete: '/options_sets/delete',
};

//  ORDERS
export const urlOrder = {
  //  add order
  create: '/orders/create',
  //  get all orders
  readAll: '/orders/read_all',
  //  get current order
  readCurrent: '/orders/read_current',
  //  get orders by status
  readByStatus: '/orders/read_by_status',
  //  change order
  update: '/orders/update',
  //  delete order
  delete: '/orders/delete',
};

//  ORDER PRIORITIES
export const urlOrderPriorities = {
  // //  add order
  // create: '/orders/create',
  //  get all order priorities
  readAll: '/order_priorities/read_all',
  // //  get current order
  // readCurrent: '/orders/read_current',
  // //  change order
  // update: '/orders/update',
  // //  delete order
  // delete: '/orders/delete',
};

//  ORDER STATUSES
export const urlOrderStatuses = {
  // //  add order
  // create: '/orders/create',
  // //  get all orders
  readAll: '/order_statuses/read_all',
  // //  get current order
  // readCurrent: '/orders/read_current',
  // //  change order
  // update: '/orders/update',
  // //  delete order
  // delete: '/orders/delete',
};

//  PAYMENTS
export const urlPayment = {
  //  add payment
  create: '/payments/create',
  //  get all payments
  readAll: '/payments/read_all',
  //  get current payment
  readCurrent: '/payments/read_current',
  //  change payment
  update: '/payments/update',
  //  delete payment
  delete: '/payments/delete',
  //  change priority delivery
  changePriority: '/payments/change_priority',
};

//  PAYPAL
export const urlPaypal = {
  //  add payment
  checkStatus: '/paypal/check_status',
  //  get all payments
//   readAll: '/payments/read_all',
//   //  get current payment
//   readCurrent: '/payments/read_current',
//   //  change payment
//   update: '/payments/update',
//   //  delete payment
//   delete: '/payments/delete',
//   //  change priority delivery
//   changePriority: '/payments/change_priority',
};

//  PRODUCTS
export const urlProduct = {
  //  add product
  create: '/products/create',
  //  get all products
  readAll: '/products/read_all',
  //  get all products
  readAllShort: '/products/read_all_short',
  //  get all products by category
  readAllByCategory: '/products/read_all_by_category',
  //  get current product
  readCurrent: '/products/read_current',
  //  change product
  update: '/products/update',
  //  delete product
  delete: '/products/delete',
};

//  PRODUCT SHADOWS
export const urlProductShadow = {
  //  add product shadow
  create: '/product_shadows/create',
  //  get all product shadows
  readAll: '/product_shadows/read_all',
  //  get current product
  readCurrent: '/product_shadows/read_current',
  //  change product shadow
  update: '/product_shadows/update',
  //  delete product shadow
  delete: '/product_shadows/delete',
};

//  PURCHASES
export const urlPurchase = {
  //  add purchase
  create: '/purchases/create',
  //  get all purchases
  readAll: '/purchases/read_all',
  //  get current purchase
  readCurrent: '/purchases/read_current',
  //  change purchase
  update: '/purchases/update',
  //  delete purchase
  delete: '/purchases/delete',
};

//  REGIONS
export const urlRegion = {
  //  add region
  create: '/regions/create',
  //  get all regions
  readAll: '/regions/read_all',
  //  get all regions
  readAllByCountryID: '/regions/read_all_by_country_id',
  //  get current region
  readCurrent: '/regions/read_current',
  //  change region
  update: '/regions/update',
  //  delete region
  delete: '/regions/delete',
};

//  ROLES
export const urlRole = {
  //  add role
  create: '/roles/create',
  //  get all roles
  readAll: '/roles/read_all',
  //  get current role
  readCurrent: '/roles/read_current',
  //  change role
  update: '/roles/update',
  //  delete role
  delete: '/roles/delete',
};

//  SBERBANK
export const urlSberbank = {
  //  add payment
  checkStatus: '/sberbank/check_status',
  //  get all payments
//   readAll: '/payments/read_all',
//   //  get current payment
//   readCurrent: '/payments/read_current',
//   //  change payment
//   update: '/payments/update',
//   //  delete payment
//   delete: '/payments/delete',
//   //  change priority delivery
//   changePriority: '/payments/change_priority',
};

//  USERS
export const urlUser = {
  //  add user
  create: '/users/create',
  //  get all users
  readAll: '/users/read_all',
  //  get current user
  readCurrent: '/users/read_current',
  //  change user
  update: '/users/update',
  //  delete user
  delete: '/users/delete',
};
