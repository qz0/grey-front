import {Image, ImageServer} from './image.types';
import {LocalizedDescription} from './localization.types';
//  PAYMENT
//  payment client
export interface Payment {
  id?: number; // ID
  localizedDescriptions: LocalizedDescription[]; //  локализованное описание
  image: Image; //  картинка
  discounts: PaymentDiscount[];
  enabled: boolean; //  видимость
  priority: number; //  приоритеты
}
//  payment server
export interface PaymentServer {
  ID?: number; // ID
  localizedDescriptions: LocalizedDescription[]; //  локализованное описание
  image: ImageServer; //  картинка
  discounts: PaymentDiscount[];
  enabled: boolean; //  видимость
  priority: number; //  приоритеты
}

//  PAYMENT DISCOUNT
//  payment discount client
export interface PaymentDiscount {
  id?: number; // ID
  shop: string; //  название магазина
  discount: number; //  размер скидки
  enabled: boolean; //  активно?
}
//  payment discount server
export interface PaymentDiscountServer {
  ID?: number; // ID
  shop: string; //  название магазина
  discount: number; //  размер скидки
  enabled: boolean; //  активно?
}

//  айтемс чека тинькова
export interface TinkoffItems {
  Name: string;
  Price: number;
  Quantity: number;
  Amount: number;
  PaymentMethod?: string;
  PaymentObject?: string;
  Tax?: string;
}

//  чек тинькова
export interface TinkoffReceipt {
  Email: string;
  Phone: string;
  EmailCompany: string;
  Taxation: string;
  Items: TinkoffItems[];
}

//  тиньков payment request
export interface TinkoffPaymentRequest {
  TerminalKey: string;
  Amount: string;
  OrderId: string;
  Description: string;
  DATA: TinkoffDataRequest;
  Receipt: TinkoffReceipt;
}

//  тиньков дата рефуест
export interface TinkoffDataRequest {
  Phone: string;
  Email: string;
}

//  тиньков дата рефуест
export interface TinkoffInitResponse {
  Success: boolean;
  ErrorCode: string;
  TerminalKey: string;
  Status: string;
  PaymentId: string;
  OrderId: string;
  Amount: number;
  PaymentURL: string;
}
