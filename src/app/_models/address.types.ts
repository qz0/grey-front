import {LocalizedDescription} from './localization.types';

//  address
//  клиентский тип для рангов
export interface Address {
  id?: number; //  ID
  zip: string; //  название
  country: Country; //  страна
  region: Region; //  регион
  city: string; //  город
  company?: string; //  название компании
  description: string; //  описание
  extended?: string[]; //
  firstName: string;  //  имя получателя
  lastName: string;  //  имя получателя
  phone: string;  //  имя получателя
  email: string;  //  имя получателя
  comments: string;  //  имя получателя
}

//  серверный тип для рангов
export interface AddressServer {
  ID?: number; //  ID
  zip: string; //  название
  country: CountryServer; //  страна
  region: RegionServer; //  регион
  city: string; //  город
  company?: string; //  название компании
  description: string; //  описание
  extended?: string[]; //
  firstName: string;  //  имя получателя
  lastName: string;  //  имя получателя
  phone: string;  //  имя получателя
  email: string;  //  имя получателя
  comments: string;  //  имя получателя
}

//  Country
//  клиентский тип для countries
export interface Country {
  id: number; //  ID
  countryID?: number; //  country_id
  localizedDescriptions: LocalizedDescription[];
  name?: string; //  название латиницой
  nameRu?: string; //  название на русском
  code2?: string; //  iso code2
  code3?: string; //  iso code3
  regions?: Region[];  //  список регионов страны
  status?: boolean;  //  какой-то статус
}

//  серверный тип для countries
export interface CountryServer {
  ID: number; //  ID
  countryID: number; //  country_id
  localizedDescriptions: LocalizedDescription[];
  name: string; //  название латиницой
  nameRU: string; //  название на русском
  code2: string; //  iso code2
  code3: string; //  iso code3
  regions: RegionServer[];  //  список регионов страны
  status: boolean;  //  какой-то статус
}

//  Country Short
//  клиентский тип для countries
export interface CountryShort {
  id: number; //  ID
  title?: string;
  localizedDescriptions: LocalizedDescription[];
}

//  серверный тип для countries
export interface CountryShortServer {
  ID: number; //  ID
  name: string; //  название латиницой
  nameRU: string; //  название на русском
}

//  REGION
//  клиентский тип для countries
export interface Region {
  id: number; //  ID
  name: string; //  название
  country: Country; //  страна
  code: string;
}
//  клиентский тип для countries
export interface RegionShort {
  id: number; //  ID
  name: string; //  название
  country: Country; //  страна
  code: string;
}

//  серверный тип для countries
export interface RegionServer {
  ID: number; //  ID
  name: string; //  название
  country: CountryServer; //  страна
  code: string;
}

