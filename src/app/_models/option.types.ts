//  типы данных для работы с опциями
//  клиентский тип для опций
import {LocalizedDescription, LocalizedDescriptionServer} from './localization.types';
import {Image, ImageServer} from './image.types';

export interface Option {
  id?: number; //  ID
  localizedDescriptions: LocalizedDescription[]; //  локализованное описание
  priority: number; //  приоритет
  type: OptionType; //  тип опции
  enabled: boolean; //  включено / выключено
  optionValues: OptionValue[]; //  варианты значений опции
}
//  серверный тип для опций
export interface OptionServer {
  ID?: number; //  ID
  localizedDescriptions: LocalizedDescriptionServer[]; //  локализованное описание
  priority: number; //  приоритет
  type: OptionTypeServer; //  тип опции
  enabled: boolean; //  включено / выключено
  optionValues: OptionValueServer[]; //  варианты значений опции
}

//  клиентский тип для значения опции
export interface OptionValue {
  id?: number; //  ID
  image: Image; // картинка
  us: string; //  US
  int: string;  //  INT
  ru: string; //  RU
  inch: string; //  INCH
  cent: string; //  CENT
  localizedDescriptions: LocalizedDescription[]; //  локализованное описание
  enabled: boolean; //  видимость
  priority: number; //  приоритет
  optionTypeID?: number; //  ид типа опции
  sizeType?: string; //  тип размера для отображения в корзине
}
//  серверный тип для значения опции
export interface OptionValueServer {
  ID?: number; //  ID
  image: Image; // картинка
  us: string; //  US
  int: string;  //  INT
  ru: string; //  RU
  inch: string; //  INCH
  cent: string; //  CENT
  localizedDescriptions: LocalizedDescriptionServer[]; //  локализованное описание
  enabled: boolean; //  видимость
  priority: number; //  приоритет
}

//  типы данных для работы с пакетами опций
//  клиентский тип для пакетов опций
export interface OptionsSet {
  id?: number; //  ID
  options: Option[];  //  набор опций
  weight: number; //  вес
  volumeWeight: number; //  объемный вес
  price: number;  //  цена
  article: string; // артикль производителя
  enabled: boolean; //  видимость
  image: Image;  //  картинка
}
//  серверный тип для пакетов опций
export interface OptionsSetServer {
  ID?: number; //  ID
  options: OptionServer[];  //  набор опций
  weight: number; //  вес
  volumeWeight: number; //  объемный вес
  price: number;  //  цена
  article: string; // артикль производителя
  enabled: boolean; //  видимость
  image: ImageServer;  //  картинка
}

//  типы данных для работы с типом опции
//  клиентский тип для опций
export interface OptionType {
  id: number; //  ID
  name: string; //  название типа опции
}
//  серверный тип для опций
export interface OptionTypeServer {
  ID: number; //  ID
  name: string; //  название типа опции
}
