//  запросы API Сбербанка

//  register request
export interface SberbankRegisterRequest {
  userName: string;
  password: string;
  orderNumber: string;
  amount: number;
  returnURL: string;
}

//  register response
export interface SberbankRegisterResponse {
  userName: string;
  password: string;
  orderNumber: string;
  amount: number;
  returnURL: string;
}
