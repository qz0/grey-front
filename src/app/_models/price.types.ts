//  типы данных для работы с пользователями
//  клиентский тип для цены
export interface Price {
  id?: number;
  buy: number;
  sell: number;
}
//  серверный тип для цены
export interface PriceServer {
  ID?: number;
  buy: number;
  sell: number;
}
