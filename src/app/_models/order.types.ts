//  ORDER STATUS
import {Address, AddressServer} from './address.types';
import {Sale, SaleServer} from './sale.types';
import {Currency, CurrencyServer, CurrencyShadow, CurrencyShadowServer} from './currency.types';
import {Delivery, DeliveryServer} from './delivery.types';
import {Item, ItemServer, ItemShadow, ItemShadowServer} from './item.types';
import {Shop, ShopServer, ShopShadow, ShopShadowServer} from './shop.types';
import {Customer, CustomerServer} from './customer.types';
import {Payment, PaymentServer} from './payment.types';
import {CartItem, CartItemServer} from './cart.types';

//  ORDER
//  клиентский тип
export interface Order {
  id?: number;  //  id
  createdAt?: Date;  //  дата создания
  updatedStatusAt?: Date;  //  дата создания
  orderID: string;  //  Идентификатор для людей
  cartItems: CartItem[];  //  отдельный товар
  items?: Item[];  //  список айтемов
  status: OrderStatus; // статус заказа
  statusID: number; // ID статус заказа
  customer: Customer; //  покупатель
  createdShippingAddress: Address; //  изначальный адрес доставки (взять оттуда поле комментария пользователя)
  currentShippingAddress: Address;  //  измененный адрес доставки
  comments: string; //  комментарий
  delivery: Delivery; //  доставка
  deliveryID: number; //  ID доставка
  deliveryPrice: number;  //  цена доставки
  deliveryPackageNumber: number; //  номер пакетов в заказе
  deliveryPackageCount: number; //  количество пакетов в заказе
  warehouseBoxNumber: number; //  номер коробки на складе
  weight: number; //  вес ордера
  payment: Payment; //  оплата
  paymentID: number; //  ID оплата
  paymentURL: string; //  URL для оплаты
  shop: Shop; //  shop
  //  history
  trackingNumber: string; //  номер отслеживания
  priority: OrderPriority; //  приоритет заказа
  priorityID: number; //  ID - приоритет заказа
  sale: Sale; //  скидка
  deliveryCost: number;  //  итоговая цена доставки
  productsCost: number;  //  итоговая цена товара
  totalCost: number;  //  итоговая цена
  currency: Currency;  //  валюта
  currencyID: number;  //  ид валюты
  //  shadow
  shippingAddressShadow: Address;  //  измененный адрес доставки
  itemsShadow?: ItemShadow[]; //  список покупок при создании
  shopShadow: ShopShadow; //  шадоу для магазина
  currencyShadow: CurrencyShadow; //  валюта на момент покупки - шадоу
}

//  серверный тип
export interface OrderServer {
  ID?: number;  //  id  createdAt?: Date;  //  дата создания
  createdAt?: Date;  //  дата создания
  updatedStatusAt?: Date;  //  дата создания
  orderID?: string;  //  Идентификатор для людей
  cartItems: CartItemServer[];  //  отдельный товар
  items?: ItemServer[];  //  список айтемов
  status?: OrderStatusServer; // статус заказа
  statusID: number; // ID статус заказа
  customer?: CustomerServer; //  покупатель
  createdShippingAddress?: AddressServer; //  изначальный адрес доставки (взять оттуда поле комментария пользователя)
  currentShippingAddress?: AddressServer;  //  измененный адрес доставки
  comments?: string; //  комментарий
  delivery: DeliveryServer; //  доставка
  deliveryID: number; //  ID доставка
  deliveryPrice: number;  //  цена доставки
  deliveryPackageNumber: number; //  номер пакетов в заказе
  deliveryPackageCount: number; //  количество пакетов в заказе
  warehouseBoxNumber: number; //  номер коробки на складе
  weight: number; //  вес ордера
  payment: PaymentServer; //  оплата
  paymentID: number; //  ID оплата
  paymentURL: string; //  URL для оплаты
  shop?: ShopServer; //  shop
  deliveryCost: number;  //  итоговая цена доставки
  productsCost: number;  //  итоговая цена товара
  totalCost: number;  //  итоговая цена товара
  currency: CurrencyServer;  //  валюта
  currencyID: number;  //  ид валюты
  //  history
  trackingNumber?: string; //  номер отслеживания
  priority?: OrderPriorityServer; //  приоритет заказа
  priorityID: number; //  ID - приоритет заказа
  sale?: SaleServer; //  скидка
  //  shadow
  itemsShadow?: ItemShadowServer[]; //  список покупок при создании
  shippingAddressShadow?: AddressServer;  //  измененный адрес доставки
  currencyShadow?: CurrencyShadowServer; //  валюта на момент покупки
  shopShadow?: ShopShadowServer; //  shop shadow
}

//  ORDER STATUS
//  клиентский тип OrderStatus
export interface OrderStatus {
  id?: number;  //  id
  name: string; //  name
}

//  серверный тип OrderStatus
export interface OrderStatusServer {
  ID?: number;  //  id
  name: string; //  name
}

//  ORDER PRIORITY STATUS
//  клиентский тип Priority
export interface OrderPriority {
  id?: number;  //  id
  name: string; //  name
}

//  серверный тип PriorityServer
export interface OrderPriorityServer {
  ID?: number;  //  id
  name: string; //  name
}

//  конфигурация карточек order
export interface OrderCardConfig {
  name: string; //  name of card
  statuses: OrderStatus[]; // statuses of order
}


