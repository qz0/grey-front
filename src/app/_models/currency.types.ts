//  Типы данных для работы с валютой
//  клиентский тип
export interface Currency {
  id?: number; //  ID
  code: string; //  идентификационный код
  title: string;  //  описание
  multiplier: number; //  множитель
  symbol: string;  //  символ валюты
  before: boolean; //  положение символа валюты
}

//  серверный тип
export interface CurrencyServer {
  // CreatedAt: string;
  // DeletedAt: string;
  ID?: number;
  // UpdatedAt: string;
  code: string;
  multiplier: number;
  title: string;
  symbol: string;  //  символ валюты
  before: boolean; //  положение символа валюты
}
//  клиентский тип
export interface CurrencyShadow {
  id?: number; //  ID
  code: string; //  идентификационный код
  title: string;  //  описание
  multiplier: number; //  множитель
}

//  серверный тип
export interface CurrencyShadowServer {
  // CreatedAt: string;
  // DeletedAt: string;
  ID?: number;
  // UpdatedAt: string;
  code: string;
  multiplier: number;
  title: string;
}
