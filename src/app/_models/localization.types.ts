//  типы данных для работы с локалью
//  клиентский тип для локализованного описания

export interface LocalizedDescription {
  id?: number; //  ID
  name?: string;  //  название
  title?: string;  //  заголовок
  description?: string; // описание
  descriptionTwo?: string; // описание
  descriptionThree?: string; // описание
  keywords?: string; //  ключевые слова
  language: string; // кодовое обозначение языка
}

//  серверный тип для локализованного описания
export interface LocalizedDescriptionServer {
  ID?: number; //  ID
  name?: string;  //  название
  title?: string;  //  заголовок
  description?: string; // описание
  descriptionTwo?: string; // описание
  descriptionThree?: string; // описание
  keywords?: string; //  ключевые слова
  language: string; // кодовое обозначение языка
}

//  localized text
//  клиентский тип для локализованного описания
export interface LocalizedText {
  id?: number; //  ID
  text?: string;  //  текст
  language: string; // кодовое обозначение языка
}

//  серверный тип для локализованного описания
export interface LocalizedTextServer {
  ID?: number; //  ID
  text?: string;  //  текст
  language: string; // кодовое обозначение языка
}

//  язык
//  клиентский тип языка
export interface Language {
  id: number; //  ID
  language: string; //  язык
  title: string; //  название
}

//  серверный тип языка
export interface LanguageServer {
  ID: number; //  ID
  language: string; //  язык
  title: string; //  название
}

//  Localization
//  клиентский тип для Localizations
export interface Localization {
  id: number; //  ID
  keyword: string; //  ключ
  localizedTexts: LocalizedText[];
  description: string; //  описание
}

//  серверный тип для Localizations
export interface LocalizationServer {
  ID: number; //  ID
  keyword: string; //  ключ
  en: string; //  текст латиницой
  ru: string; //  текс на русском
  description: string; //  описание
}
