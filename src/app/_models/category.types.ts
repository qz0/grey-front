//  Типы данных для работы с валютой
//  клиентский тип
import { Image, ImageServer } from './image.types';
import { LocalizedDescription } from './localization.types';

export interface Category {
  id?: number; //  ID
  parentID: number; //  ID родительской категории
  localizedDescriptions: LocalizedDescription[];  //  описания
  localizedSEOs: LocalizedDescription[]; //  SEO
  imageBackground: Image; //  фон
  imageIcon: Image; //  иконка
  // firstImage: Image; // первая картинка
  // secondImage: Image; // вторая картинка
  images: Image[];  //  картинки
  priority: number; //  приоритет в списке
  seoLink: string;  //  сео линк
  categories: Category[];  //  дети
  url: string;  //  url
  isEnabled: boolean; // видимость
  type: string; // тип
}
//  server
export interface CategoryServer {
  ID?: number; //  ID
  parentID: number; //  ID родительской категории
  localizedDescriptions: LocalizedDescription[];  //  описания
  localizedSEOs: LocalizedDescription[]; //  SEO
  imageBackground: ImageServer; //  фон
  imageIcon: ImageServer; //  иконка
  // firstImage: ImageServer; // первая картинка
  // secondImage: ImageServer; // вторая картинка
  images: ImageServer[];  //  картинки
  priority: number; //  приоритет в списке
  seoLink: string;  //  сео линк
  categories: CategoryServer[];  //  дети
  url: string;  //  url
  isEnabled: boolean; // видимость
  type: string; // тип
}
