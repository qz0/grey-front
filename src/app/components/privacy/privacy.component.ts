import { Component, OnInit, OnDestroy } from '@angular/core';
import {Language, Localization} from '../../_models/localization.types';
import {Subscription} from 'rxjs';
import {DataService} from '../../_services/data.service';

@Component({
  selector: 'app-privacy',
  templateUrl: './privacy.component.html',
  styleUrls: ['./privacy.component.scss']
})
export class PrivacyComponent implements OnInit, OnDestroy {

  // public
  public languageString: string = 'EN';
  public localizations: Localization[] = null; // список локализаций

  //  private
  private languageSub: Subscription;
  private localizationsSub: Subscription; //  подписка на список локализаций

  //  constructor
  constructor(private dataService: DataService) {
    //  подписываемся на язык
    this.languageSub = this.dataService.castLanguage.subscribe((language: Language) => {
      if (language) {
        // console.log('Have changes :', language);
        this.languageString = language.language;
        console.log(this.languageString);
      }
    });

    //  подписка localizations
    this.localizationsSub = this.dataService.castLocalizations.subscribe((localizations: Localization[]) => {
      this.localizations = localizations;
    });
}

  ngOnInit() {
  }

  //  destroy
  public ngOnDestroy() {
    // отписываемся от localizations
    this.localizationsSub.unsubscribe();
  }

}
