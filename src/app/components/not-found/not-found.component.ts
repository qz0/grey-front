import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.scss']
})
export class NotFoundComponent implements OnInit {

  constructor(private activatedRoute: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    // const stringForSearch = this.activatedRoute.snapshot.url;
    // console.log('stringForSearch', stringForSearch);
    // this.router.navigate(['/products/product/1'], { skipLocationChange: true });
  }

}
