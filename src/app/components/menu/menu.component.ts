import {Component, OnDestroy, OnInit, ViewChild, ElementRef, ChangeDetectorRef, Renderer2} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { Animations } from '../../animations';
import { MenuAnimations } from './menu.animations';


import {Menu} from '../../_models/menu.types';
import {Subscription} from 'rxjs';
import {AuthService} from '../../_services/auth.service';
import {DataService} from '../../_services/data.service';
import {Category} from '../../_models/category.types';
import {Language, Localization} from '../../_models/localization.types';
import {Currency} from '../../_models/currency.types';
import {Cart, CartItem} from '../../_models/cart.types';

@Component({
  selector: 'app-menu',
  animations: [Animations.fadeInOut, MenuAnimations.overlay, MenuAnimations.showHideSearch, MenuAnimations.showDropdownBlock],
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit, OnDestroy {
  // public
  public collapsed: boolean = true; // show/hide menu
  public logoPath = '/assets/img/logo.svg'; // logo
  public menu: Menu[] = null; // menu list
  public cart: Cart = null; // cart
  public cartItemsAmount: number = 0; //  количество элементов в корзине
  public categories: Category[] = null; // categories list
  public isLogon: boolean = null;  //  фактор логина
  public showCart: boolean = false; // show / hide cart
  public showSubMenu: boolean = false; // show / hide submenu
  public showMobileMenu: boolean = false; // show / hide mobile menu
  public showSearch: boolean = false; // show / hide search input
  public showSettingsToggles = false; // show / hide settings toggles
  public showMobileSettingsToggles = false; // show / hide mobile settings toggles
  public showSearchResults: boolean = false; // show / hide search results input
  public searchInputValue: string = ''; // символы в строке поиска
  public selectedCurrency: string = 'usd'; // выбранная валюта
  public selectedLanguage: string = 'ru'; // выбранный язык
  public currency: Currency = null; // выбранная валюта
  public currencies: Currency[] = null; // список валют
  public localizations: Localization[] = null; // список локализаций
  public language: Language = null; // выбранный язык
  public languages: Language[] = null; // список языков

  @ViewChild('inputBlock', {static: false})inputBlock: ElementRef; // Поиск - поле ввода

  //  private
  private menuSub: Subscription; //  subscriptions
  private cartSub: Subscription; //  cart sub
  private categoriesSub: Subscription; //  subscriptions
  private cartIsShownBitSub: Subscription; //  подписка на бит отображения корзины
  private languageSub: Subscription; //  подписка на текущий язык
  private localizationsSub: Subscription; //  подписка на список локализаций
  private currencySub: Subscription; //  подписка на текущую валюту
  private languagesSub: Subscription; //  подписка на список языков
  private currenciesSub: Subscription; //  подписка на список валют

  //  constructor
  constructor(
    private dataService: DataService,
    private authService: AuthService,
    private changeDetectorRef: ChangeDetectorRef,
    private router: Router,
    private renderer: Renderer2) {
    //  подписки
    //  подписка menu
    this.menuSub = this.dataService.castMenu.subscribe((menu: Menu[]) => {
      this.menu = menu;
    });

    //  подписка cart
    this.cartSub = this.dataService.castCart.subscribe((cart: Cart) => {
      if (cart) {
        //   если что-то есть - возвращаем число
        this.cartItemsAmount = 0;
        if (cart.cartItems && cart.cartItems.length) {
          //  пробегаемся по всем айтемам чтобы суммировать количество
          cart.cartItems.forEach((cartItem: CartItem) => {
            this.cartItemsAmount += cartItem.orderCount;
          });
        }
        this.cart = cart;
      }
    });

    //  подписка categories
    this.categoriesSub = this.dataService.castCategoriesTop.subscribe((categories: Category[]) => {
      this.categories = categories;
    });

    //  подписка cartIsShown
    this.cartIsShownBitSub = this.dataService.castCartIsShownBit.subscribe((cartISShownMenu: boolean) => {
      this.showCart = cartISShownMenu;
    });

    //  подписка currency
    this.currencySub = this.dataService.castCurrency.subscribe((currency: Currency) => {
      this.currency = currency;
    });

    //  подписка currency
    this.currenciesSub = this.dataService.castCurrencies.subscribe((currencies: Currency[]) => {
      this.currencies = currencies;
    });

    //  подписка language
    this.languageSub = this.dataService.castLanguage.subscribe((language: Language) => {
      this.language = language;
      console.log('language has changed');
      //  обновляем локализацию стран
      // this.dataService.getCountriesShort(language.language);
    });

    //  подписка localizations
    this.localizationsSub = this.dataService.castLocalizations.subscribe((localizations: Localization[]) => {
      this.localizations = localizations;
    });

    //  подписка languages
    this.languagesSub = this.dataService.castLanguages.subscribe((languages: Language[]) => {
      this.languages = languages;
    });

    // подписка на изменение route link
    this.router.events.subscribe(() => {
      if (this.showSearch === true) {
        this.invertShowSearch();
      }
      if (this.showCart === true) {
        this.invertShowCart();
      }
      if (this.showSettingsToggles === true) {
        this.invertSettingsToggles();
      }
  });
  }

  //  дестроер
  ngOnDestroy() {
    //  отписки
    this.menuSub.unsubscribe();
    this.cartIsShownBitSub.unsubscribe();
    this.cartSub.unsubscribe();
    this.categoriesSub.unsubscribe();
    this.currencySub.unsubscribe();
    this.currenciesSub.unsubscribe();
    this.languageSub.unsubscribe();
    this.languagesSub.unsubscribe();
    this.localizationsSub.unsubscribe();
    // this.isLoginSubscription.unsubscribe();
  }

  //  инитор
  ngOnInit() {
    //  подписка на логин
    // this.isLoginSubscription = this.authService.castIsLogon.subscribe((isLogon: boolean) => {
    //   this.isLogon = isLogon;
    // });
    //  дергаем обновление меню
    this.dataService.getMenu();
  }

  // show/hide menu
  public invertCollapse() {
    this.collapsed = !this.collapsed;
  }

  // show/hide cart
  public invertShowCart() {
    this.dataService.changeCartVisibility(!this.showCart);
    // this.showCart = !this.showCart;

    // отключить прокрутку фона при открытой корзине
    if (this.showCart) {
      this.renderer.addClass(document.body, 'no-scroll');
    } else {
      this.renderer.removeClass(document.body, 'no-scroll');
    }

    if (this.showMobileMenu) {
      this.showMobileMenu = false;
    }
  }

  // show/hide mobile menu
  public invertShowMobileMenu() {
    this.showMobileMenu = !this.showMobileMenu;
    this.showMobileSettingsToggles = false;

    // отключить прокрутку фона при открытом меню
    if (this.showMobileMenu) {
      this.renderer.addClass(document.body, 'no-scroll');
    } else {
      this.renderer.removeClass(document.body, 'no-scroll');
    }

    if (this.showCart) {
      this.showCart = false;
    }
  }

  // show submenu
  public showNestedMenu() {
    this.showSubMenu = true;
  }

  // hide submenu
  public hideNestedMenu() {
    this.showSubMenu = false;
  }

  // show/hide search input
  public invertShowSearch() {
    this.showSearch = !this.showSearch;
    if (this.showSearch === false) {
      this.searchInputValue = '';
      this.showSearchResults = false;
    } else {
      this.changeDetectorRef.detectChanges();
      setTimeout(() => {
        this.inputBlock.nativeElement.focus();
      }, 300);
    }
  }

  // show search results
  public searchResults() {
    if (this.searchInputValue.length > 2) {
      this.showSearchResults = true;
    } else {
      this.showSearchResults = false;
    }
  }

  // show/hide settings toggles
  public invertSettingsToggles() {
    this.showSettingsToggles = !this.showSettingsToggles;
  }

  public invertMobileSettings() {
    this.showMobileSettingsToggles = !this.showMobileSettingsToggles;
  }

  public chooseCurrency(currency: string) {
    this.selectedCurrency = currency;
  }

  public chooseLanguage(language: string) {
    this.selectedLanguage = language;
  }

  //  задать валюту
  public setCurrency(currency: Currency) {
    //  дергаем сервис
    this.dataService.setCurrency(currency);
  }

  //  задать язык
  public setLanguage(language: Language) {
    //  дергаем сервис
    this.dataService.setLanguage(language);
  }

  //  количество элементов в корзине
  public getCartCount(): number {
    //   если что-то есть - возвращаем число, нет - null
    if (this.cart && this.cart.cartItems && this.cart.cartItems.length) {
      return this.cart.cartItems.length;
    } else {
      return null;
    }
  }
}
