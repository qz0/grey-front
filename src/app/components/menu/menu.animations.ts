import { trigger, state, style, transition, animate } from '@angular/animations';

export const MenuAnimations = {
  // затемнение экрана
  overlay: trigger('overlay', [
    transition(':enter', [
      style({ opacity: 0, }),
      animate('.2s', style({ opacity: 1 })),
    ]),
    transition(':leave', [
      animate('.2s', style({ opacity: 0 }))
    ])
  ]),
  // открыть / закрыть поле поиска
  showHideSearch: trigger('showHideSearch', [
    transition(':enter', [
      style({transform: 'translateX(50rem)', opacity: 0}),
      animate('.3s ease', style({ transform: 'translateX(0)', opacity: 1 })),
    ]),
    transition(':leave', [
      style({transform: 'translateX(0)', opacity: 1 }),
      animate('.3s ease', style({transform: 'translateX(50rem)', opacity: 0 }))
    ])
  ]),
  // окно выезжающее вниз
  showDropdownBlock: trigger('showDropdownBlock', [
    transition(':enter', [
      style({height: 0, padding: 0}),
      animate('.3s ease', style({ height: '9.625rem', padding: '1.25rem'})),
    ]),
    transition(':leave', [
      style({height: '9.625rem', padding: '1.25rem' }),
      animate('.2s ease', style({height: 0, padding: 0}))
    ])
  ])
};
