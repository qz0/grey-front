import { Component, OnInit } from '@angular/core';
import {ICreateOrderRequest, IPayPalConfig} from 'ngx-paypal';
import {ActivatedRoute, Router} from '@angular/router';
import {Language} from '../../_models/localization.types';
import {Subscription} from 'rxjs';
import {DataService} from '../../_services/data.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  //  public
  //  нужно для прелоада payment
  public payPalConfig?: IPayPalConfig;
  public showPayment: boolean = true;
  public amount: string = '0';
  public languageString: string = 'EN';

  //  private
  private languageSub: Subscription;

  constructor(private router: Router, private dataService: DataService) {
    //  подписываемся на язык
    this.languageSub = this.dataService.castLanguage.subscribe((language: Language) => {
      if (language) {
        // console.log('Have changes :', language);
        this.languageString = language.language;
        console.log(this.languageString)
      }
    });
  }

  ngOnInit() {
    //  запуск останов паэмента
    this.initConfig();
    this.hidePaypal();
  }

  //  preloading payment


  private initConfig(): void {
    this.payPalConfig = {
      currency: 'EUR',
      clientId: 'AXc_EI6-eB-7LEHLQ0uBujO6LbIFT7srHv9SNxTiW2AdLMCr7PmXLhgf1UQA9lKDCfNFOHFZeczATsVy',
      // clientId: 'AXc_EI6-eB-7LEHLQ0uBujO6LbIFT7srHv9SNxTiW2AdLMCr7PmXLhgf1UQA9lKDCfNFOHFZeczATsVy',
      // tslint:disable-next-line
      createOrderOnClient: (data) => <ICreateOrderRequest> {
        intent: 'CAPTURE',
        purchase_units: [
          {
            amount: {
              currency_code: 'EUR',
              value: this.amount,
              breakdown: {
                item_total: {
                  currency_code: 'EUR',
                  value: this.amount
                }
              }
            },
            items: [
              {
                name: 'Enterprise Subscription',
                quantity: '1',
                category: 'DIGITAL_GOODS',
                unit_amount: {
                  currency_code: 'EUR',
                  value: this.amount,
                },
              }
            ]
          }
        ]
      },
      advanced: {
        commit: 'true'
      },
      style: {
        label: 'paypal',
        layout: 'vertical',
        size: 'large',
        // shape: 'pill',
        color: 'silver',
        // tagline: false
      },
      onApprove: (data, actions) => {
        console.log('onApprove - transaction was approved, but not authorized', data, actions);
        actions.order.get().then(details => {
          console.log('onApprove - you can get full order details inside onApprove: ', details);
        });
      },
      onClientAuthorization: (data) => {
        console.log('onClientAuthorization - you should probably inform your server about completed transaction at this point', data);
      },
      onCancel: (data, actions) => {
        console.log('OnCancel', data, actions);
      },
      onError: err => {
        console.log('OnError', err);
      },
      onClick: (data, actions) => {
        console.log('onClick', data, actions);
      },
    };
  }



  //  отшибалка для пэйпэла
  private hidePaypal() {

    const that = this;

    setTimeout(() => {
      that.showPayment = false;
    }, 3000);

  }
}
