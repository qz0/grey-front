import { Component, OnInit, OnDestroy } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Language, Localization} from '../../_models/localization.types';
import {Subscription} from 'rxjs';
import {DataService} from '../../_services/data.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit, OnDestroy {
  public tellUsFormGroup: FormGroup;
  public localizations: Localization[] = null; // список локализаций

  private localizationsSub: Subscription; //  подписка на список локализаций

  constructor(private formBuilder: FormBuilder, private dataService: DataService) {
    //  подписка localizations
    this.localizationsSub = this.dataService.castLocalizations.subscribe((localizations: Localization[]) => {
      this.localizations = localizations;
    });
  }

  ngOnInit() {
    // инициализация формы
    this.tellUsFormGroup = this.formBuilder.group({
      email: [null, [Validators.required, Validators.email]],
      message: [null,[Validators.required, Validators.maxLength(1800)]],
      recaptcha: ['', Validators.required]
    });
  }

  ngOnDestroy() {
    this.localizationsSub.unsubscribe();
  }

  onSubmit() {
    // const controls = this.tellUsFormGroup.controls;

    // Проверяем форму на валидность
    if (this.tellUsFormGroup.invalid) {
      return;
    }

    // TODO: Обработка данных формы
    console.log(this.tellUsFormGroup.value);
    }

}
